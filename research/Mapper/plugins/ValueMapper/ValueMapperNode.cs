#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using SlimDX;
using SlimDX.Direct3D9;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "Mapper", Category = "Value", Help = "Basic template with one value in/out", Tags = "")]
	#endregion PluginInfo
	public class ValueMapperNode : IPluginEvaluate
	{
		#region fields & pins
		Dictionary<string, Shape> SourceShapeList =  new Dictionary<string, Shape>();
		
		Dictionary<string, Fixture> Fixtures =  new Dictionary<string, Fixture>();
		
		Fixture fixture;
		Shape myShape;
		int shapeIndex;
		bool dragShape = false;
		bool dragHandle = false;
		string dragId = "nil";
		int dragHandleIndex;
		double offsetX;
		double handleSize = 0.05;
		int lastFrameClick;
		int currentClick;
		bool upEdge;
		bool downEdge;
		bool scaleShape;
		
		Vector2D OP00 = new Vector2D(-.5, .5);
		Vector2D OP01 = new Vector2D(.5, .5);
		Vector2D OP02 = new Vector2D(.5, -.5);
		Vector2D OP03 = new Vector2D(-.5, -.5);
		
		[Input("AddShape", IsBang = true, IsSingle = true)]
		ISpread<bool> FAddShape;
		
		[Input("Mouse (xyz)")]
		IDiffSpread<Vector3D> FMouse;
		
		[Input("Initialize", IsBang = true, IsSingle = true)]
		ISpread<bool> FInit;
		
		[Output("Original Points")]
		ISpread<Vector2D> FOriginalPoints;
		
		[Output("Handles")]
		ISpread<Vector2D> FHandlesOut;
		
		[Output("Key")]
		ISpread<string> FKey;
		
		[Output("DragId")]
		ISpread<string> FDragId;
		
		[Output("MouseOverShape")]
		ISpread<bool> FMouseOverShape;
		
		[Output("MouseOverHandle")]
		ISpread<bool> FMouseOverHandle;
		
		[Output("", Visibility = PinVisibility.False)]
		ISpread<Vector2D> LastFrameValue;
		
		
		[Import()]
		ILogger FLogger;
		#endregion fields & pins
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			currentClick = (int)FMouse[0].z;
			if(currentClick > lastFrameClick)
			{
				upEdge=true;
				downEdge=false;
			}
			else if (currentClick < lastFrameClick)
			{
				upEdge=false;
				downEdge=true;
			}
			else
			{
				upEdge=false;
				downEdge=false;
			}
			Vector2D currentInput = (new Vector2D(FMouse[0].x, FMouse[0].y));
			
			FOriginalPoints.SliceCount = 4;
			
			FOriginalPoints[0] = OP00;
			FOriginalPoints[1] = OP01;
			FOriginalPoints[2] = OP02;
			FOriginalPoints[3] = OP03;
			
			if (FInit[0])
			SourceShapeList.Clear();
			
			if (FAddShape[0])
			{
				myShape = new Shape(OP00, OP01, OP02, OP03);
				SourceShapeList.Add("Shape0" + shapeIndex.ToString(), myShape);
			}
			
			shapeIndex = SourceShapeList.Count;
			
			
			if (FMouse.IsChanged)
			{
				
				foreach (var shape in SourceShapeList)
				{
					// HIT TEST for Shapes
					if(pointInPolygon(shape.Value,FMouse[0].x,FMouse[0].y))
					{
						shape.Value.isMouseOver = true;
						if (upEdge && !dragShape)
						{
							
							dragShape = true;
							dragId = shape.Key;
						}
					}
					else
					shape.Value.isMouseOver = false;
					
					
					// HIT TEST for ShapeHandles
					foreach (var handle in shape.Value.quadHandles)
					{
						
						if ((FMouse[0].x > handle.x - handleSize && FMouse[0].x < handle.x + handleSize)&&
						(FMouse[0].y > handle.y - handleSize && FMouse[0].y < handle.y + handleSize))
						{
							if (upEdge && !dragShape)
							{
								dragHandle=true;
								dragId=shape.Key;
								dragHandleIndex = shape.Value.quadHandles.IndexOf(handle);
								
							}
							handle.isMouseOver=true;
							
						}
						else
						handle.isMouseOver=false;
						
						
					}
					
					
					//HITTEST for Buttons
					if ((FMouse[0].x > shape.Value.scaleButton.x - shape.Value.buttonSize && FMouse[0].x < shape.Value.scaleButton.x + shape.Value.buttonSize) &&
						(FMouse[0].y > shape.Value.scaleButton.y - shape.Value.buttonSize && FMouse[0].y < shape.Value.scaleButton.y + shape.Value.buttonSize) )
					{
						if (upEdge)
						{
							scaleShape = true;
							dragId=shape.Key;
						}
						
						
					}
					
					if (downEdge)
					{
						dragShape = false;
						dragHandle = false;
						scaleShape = false;
					}
					
				}
			}
			if ( scaleShape && !dragShape && ! dragHandle)
			{
				SourceShapeList[dragId].scale(currentInput.x-LastFrameValue[0].x);
			}
			
			if(dragShape && !dragHandle)
			{
				foreach (var handle in SourceShapeList[dragId].quadHandles)
				{
					handle.x += currentInput.x-LastFrameValue[0].x;
					handle.y +=  currentInput.y-LastFrameValue[0].y;
					
				}
			}
			if(!dragShape && dragHandle)
			{
				if(SourceShapeList[dragId].allowWarp)
				{
				SourceShapeList[dragId].quadHandles[dragHandleIndex].x += currentInput.x-LastFrameValue[0].x;
				SourceShapeList[dragId].quadHandles[dragHandleIndex].y += currentInput.y-LastFrameValue[0].y;
				}
			}
			
			
			FMouseOverShape.SliceCount = SourceShapeList.Count;
			FKey.SliceCount =SourceShapeList.Count *4;
			FHandlesOut.SliceCount = SourceShapeList.Values.Count * 4;
			FMouseOverHandle.SliceCount = SourceShapeList.Values.Count * 4;
			
			int i = 0;
			foreach (var shape in SourceShapeList)
			{shape.Value.update();
				FMouseOverShape[i] = shape.Value.isMouseOver;
				int j = 0;
				foreach (var handle in shape.Value.quadHandles)
				{
					FHandlesOut[i*4+j] = new Vector2D(handle.x, handle.y);
					FKey[j] = shape.Key;
					FMouseOverHandle[i*4+j] = handle.isMouseOver;
					j++;
				}
				i++;
				
			}
			
			lastFrameClick = (int)FMouse[0].z;
			LastFrameValue[0] = currentInput;
		}
		
		
		#region POLYGON HITTEST
		// 
		// http://alienryderflex.com/polygon/
		// 
		//  Globals which should be set before calling this function:
		//
		//  int    polySides  =  how many corners the polygon has
		//  float  polyX[]    =  horizontal coordinates of corners
		//  float  polyY[]    =  vertical coordinates of corners
		//  float  x, y       =  point to be tested
		//
		//  (Globals are used in this example for purposes of speed.  Change as
		//  desired.)
		//
		//  The function will return YES if the point x,y is inside the polygon, or
		//  NO if it is not.  If the point is exactly on the edge of the polygon,
		//  then the function may return YES or NO.
		//
		//  Note that division by zero is avoided because the division is protected
		//  by the "if" clause which surrounds it.
		
		bool pointInPolygon(Shape shape, double x, double y) 
		{
			int polySides =shape.quadHandles.Count;
			double[]polyX = new double[shape.quadHandles.Count];
			double[]polyY = new double[shape.quadHandles.Count];
			
			for ( int p=0; p< shape.quadHandles.Count; p++)
			{
				polyX[p] = shape.quadHandles[p].x;
				polyY[p] =shape.quadHandles[p].y;
			}
			int   i, j=polySides-1 ;
			bool  oddNodes=false      ;
			
			for (i=0; i<polySides; i++) {
				if ((polyY[i]< y && polyY[j]>=y
				||   polyY[j]< y && polyY[i]>=y)
				&&  (polyX[i]<=x || polyX[j]<=x)) {
					oddNodes^=(polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x); }
				j=i; }
			
			return oddNodes; 
			
		}
		#endregion POLYGON HITTEST
		
	}
	
	public class Fixture 
	{
		
	}
	
	public class Shape
	{
		[Import()]
		ILogger FLogger;
		public List<Handle> quadHandles = new List<Handle>();
		public double buttonSize = .25;
		public Handle scaleButton;
		public Handle rotateButton;
		public bool allowWarp { get; set; }
		public bool isMouseOver { get; set; }
		public Shape (Vector2D h0, Vector2D h1, Vector2D h2, Vector2D h3)
		{
			quadHandles.Add(new Handle(h0.x,h0.y));
			quadHandles.Add(new Handle(h1.x,h1.y));
			quadHandles.Add(new Handle(h2.x,h2.y));
			quadHandles.Add(new Handle(h3.x,h3.y));
			allowWarp = true;
			scaleButton = new Handle(h2.x,h0.y-((h0.y-h3.y)/2));
			rotateButton = new Handle(h0.y+((h3.x-h0.x)/2),h0.y);
		}
		public void scale (double factor)
		{
			Matrix4x4 tmp = new Matrix4x4();
			SlimDX.Vector3 scale;
			SlimDX.Vector3 translate;
			Quaternion rotate;

			foreach (var handle in quadHandles)
			{
				tmp.m14 = handle.x;
				tmp.m24 = handle.y;
				//tmp *= VMath.Scale(2,2,2);
			//	FLogger.Log(LogType.Debug, tmp.m24.ToString());
				(VMath.Scale(2,2,2)*tmp).ToSlimDXMatrix().Decompose(out scale,out rotate,out translate);
				//Decompose(out scale,out rotate,out translate);
				handle.x = translate.X;
				handle.y = translate.Y;
			}

		}
		public void update()
		{

			scaleButton.x = quadHandles[2].x;
			scaleButton.y = quadHandles[0].y-((quadHandles[0].y-quadHandles[3].y)/2);
		
			rotateButton.x = quadHandles[0].x+((quadHandles[2].x-quadHandles[0].x)/2);
			rotateButton.y = quadHandles[0].y;
			
		}
	}
	
	public class Handle
	{
		
		public bool isMouseOver { get; set; }
		public double x { get; set; }
		public double y { get; set; }
		
		
		public Handle(double _x, double _y)
		{
			x = _x;
			y = _y;
		}
	}
}
