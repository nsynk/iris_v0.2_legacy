$(function(){
	
	var url = 'http://localhost:8080';
	
	var socket = io.connect(url);

	$(document).ready(function() {
	  socket.emit('refreshdata');
	});

	socket.on('changevalue', function (data) {

		//console.log('data id = ' + data.id);
		
		id = '#' + data.id;

		$(id).val(data.val);
		$(id).slider('refresh');
		
	});
	
	var lastEmit = $.now();

	$(".slider").live('change', function(event) {
		
		if($.now() - lastEmit > 30){
			id = this.id;
		    val = $(this).val();

			socket.emit('slidervaluechanged', {
				'id': id, 
				'val': val
			})

			lastEmit = $.now();
		}

		console.log(id + ': ' + val);
	});	
	
	$("#button").click(function() {
		//var SliderValue = $("#slider1").val();
		//alert("Slider value = " + SliderValue);
		//console.log("Slider value =" + SliderValue);
		socket.emit('buttonclicked', {
			'val': -1
		})
		
	});
	
});