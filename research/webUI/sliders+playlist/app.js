// Including libraries

var app = require('http').createServer(handler),
	io = require('socket.io').listen(app),
	static = require('node-static'), // for serving files
	dgram = require('dgram'),
	osc = require('node-osc');

var oscClient = new osc.Client('10.0.1.35', 3333);
var oscServer = new osc.Server(9020, '0.0.0.0');

var fileServer = new static.Server('./');

var sliderdata = [];
app.listen(8080);

function handler (request, response) {

	request.addListener('end', function () {
        fileServer.serve(request, response);
    });
}

// Delete this row if you want to see debug messages
io.set('log level', 1);

// Listen for incoming connections from clients
io.sockets.on('connection', function (socket) {

	socket.on('slidervaluechanged', function (data) {

		sliderdata[data.id] = data.val;
		
		console.log('slider ' + data.id + ' changed to ' + data.val);
		
		socket.broadcast.emit("changevalue", data);
		
		oscClient.send('/67/11/Y Input Value', data.val);

				
	});

	socket.on('refreshdata', function () {

		for (var prop in sliderdata) {
		  if (sliderdata.hasOwnProperty(prop)) { 
		    console.log("prop: " + prop + " value: " + sliderdata[prop]);
			socket.emit('changevalue', {
				'id': prop, 
				'val': sliderdata[prop]
			})
		  }
		}
		
		for (var i = 0; i < sliderdata.length; i++) {

			socket.emit('changevalue', {
				'id': id, 
				'val': val
			})
		}		

	});
	
	socket.on('buttonclicked', function (data) {
		
		console.log('button clicked with value ' + data.val);
		
		//socket.broadcast.emit("changevalue","100");
		
		
		console.log(sliderdata);
		
		//socket.broadcast.emit("changevalue", sliderdata['slider1']);
		console.log('sliderdata length = ' + sliderdata.length);
		
		for (var prop in sliderdata) {
		  if (sliderdata.hasOwnProperty(prop)) { 
		    console.log("prop: " + prop + " value: " + sliderdata[prop]);
			socket.emit('changevalue', {
				'id': prop, 
				'val': sliderdata[prop]
			})
		  }
		}
		
		for (var i = 0; i < sliderdata.length; i++) {

			//id = sliderdata[i][0];
			//val = sliderdata[i][1];
			
			console.log(sliderdata[i]);
			
			//console.log('emitting id ' + id + ' to ' + val);

			socket.emit('changevalue', {
				'id': id, 
				'val': val
			})
		}		
		/*
		var sendingMsg = setTimeout(function(){
			socket.broadcast.emit("changevalue","100");
		},1000);
		*/

	});

	oscServer.on("message", function (msg, rinfo) {
		console.log(msg);
		var val = msg[2][1];
		console.log(val);
		//console.log(msg[0], ":", msg[1]);
		socket.broadcast.emit("changevalue", val);

	});


	var sendingMsg = setInterval(function(){
		//socket.broadcast.emit("changevalue","100");

	},1000);


});