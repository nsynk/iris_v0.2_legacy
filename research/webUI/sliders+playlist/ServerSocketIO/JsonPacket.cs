using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace VVVV.Nodes
{
   [JsonObject(MemberSerialization.OptIn)]
   public class JsonPacket
   {
       [JsonProperty]
       public int SliceCount { get; set; }

       [JsonProperty]
       public List<string> Values { get; private set; }

       public JsonPacket()
       {
           Values = new List<string>();
       }

       public string ToJsonString()
       {
           return JsonConvert.SerializeObject(this);
       }
       public static JsonPacket Deserialize(string jsonString)
       {
           return JsonConvert.DeserializeObject<JsonPacket>(jsonString);
       }
   }
}