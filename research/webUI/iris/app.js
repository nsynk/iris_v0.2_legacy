var express = require('express')
  , routes = require('./routes')
  , config = require('./config')
  , osc = require('node-osc')
  , fs = require('fs')
  , xml2js = require('xml2js')
  ,	util = require('util')
  , patch = require('./patch')

var module_elements = config.init_elements;

var app = module.exports = express.createServer();

var oscClient = new osc.Client('10.0.1.35', 3333);
var oscServer = new osc.Server(4000, '0.0.0.0');

// Configuration

app.configure(function(){
  app.set('adminpass', config.adminpass);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.set('view options', { pretty: true });
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// ------

/*
var n = 7;
while (n--) {
  patch.pins.push({ name: 'pin' + n});
}
*/

var parser = new xml2js.Parser();
fs.readFile('interface-reduced.xml', function(err, data) {
    parser.parseString(data, function (err, result) {
		patch = result['PATCH'];
    });
});


// Routes

app.get('/', routes.index);
app.get('/iris', routes.iris);

var routes = require('./routes');
var module = require('./routes/module')

app.all('/module/:id/:op?', module.load);
app.get ('/module/:id', module.view);


var io=require('socket.io').listen(app);
app.listen(3000);
io.sockets.on('connection', function( socket ) {
	
	socket.emit('elements', module_elements);
	
	oscServer.on("message", function (msg, rinfo) {
		var message = [];
		message = msg[2];
		console.log(msg);
		
		for (var i=0; i<message.length; i++) {
			console.log(i + ": " + message[i]);
			var id = message[i].split("|")[0];
			console.log(id);
			modules[i][0]
		}
		/*
		switch (message[0]) {
			case "/exposedpins":
				module_elements.length = 0;
				for (var prop in message) {
				  if (message.hasOwnProperty(prop) && prop >= 1) {
				    //console.log("prop: " + prop + " value: " + message[prop]);
					var id = message[prop].replace('/', '-');
					console.log(id);
					if (message[prop] != 'none')
						module_elements.push('slider' + prop);
				  }
				}
				socket.emit('elements', module_elements);
				break;
			default:
				break;
		}
		*/
		
		

		/*
		module_elements.length = 0;
		console.log(module_elements);
		var i = 0;
		for (var prop in msg) {
		  if (msg.hasOwnProperty(prop)) {
		    console.log("prop: " + prop + " value: " + msg[prop]);
			if (i>1 && msg[prop] != '') module_elements[i] = msg[prop];
			i++;
		  }
		}
		console.log(module_elements);
		console.log("\n\n");
		
		
		socket.emit('elements', module_elements);
		
		*/
		
	});

	
});
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
