var fs = require('fs'),
    xml2js = require('xml2js'),
	util = require('util');

var parser = new xml2js.Parser();
fs.readFile(__dirname + '/docs/interface-reduced.xml', function(err, data) {
    parser.parseString(data, function (err, result) {
	
		//var face = JSON.stringify(result);
		//console.log(result['PATCH']);
		
		console.log(util.inspect(result['PATCH'], false, null));
		
		var patch = result['PATCH'];


		for(var node in patch){
		
			var pin = patch[node];

			for(var property in pin){
				
				//console.log(property+": "+pin[property]);
				
				//console.log(util.inspect(pin[property], false, null));
				
				var params = pin[property]['$'];
				console.log('Name: ' + params.Name);
				console.log('ID: ' + params.ID);
				//console.log(params);
				console.log('-------------');
				
				var pins = pin[property]['PIN'];
				
				for (var attributes in pins) {
					console.log('Attributes:');
					var values = pins[attributes]['$'];
					//console.log(JSON.stringify(values));
					console.log('Path: ' + values.Path);
					console.log('Type: ' + values.Type);
					console.log('SubType: ' + values.SubType);
					console.log('Tag: ' + values.Tag);
					console.log('Default: ' + values.Default);
					console.log('Min: ' + values.Min);
					console.log('Max: ' + values.Max);
					console.log('--');
				}
				
				console.log('=============');
				
			}
	
		}
		
		
		console.log('////////////////////');


    });
});