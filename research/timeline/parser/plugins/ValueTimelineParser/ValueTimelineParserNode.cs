#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "TimelineParser", Category = "Value", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	#endregion PluginInfo
	public class ValueTimelineParserNode :  IDisposable, IPluginEvaluate
	{
		#region fields & pins
		//fields
		private bool FDisposed = false;
		private INode2 FPatch;
		private bool firstFrame = true;
		private string nodePath;
		
		//collections
		
		
		private Dictionary<string, Track> tracks = new Dictionary<string, Track>();
		
		//pins
		[Input("Node Path", IsSingle = true)]
		IDiffSpread<string> FNodePath;
		
		
		[Input("Initialize", IsBang = true, IsSingle = true)]
		ISpread<bool> FInit;
		
		[Input("Update", IsBang = true, IsSingle = true)]
		ISpread<bool> FUpdate;
		
		[Output("Track Count")]
		ISpread<int> FTrackCount;
		
		[Output("Track Name")]
		ISpread<string> FTrackName;
		
		[Output("Keyframe Count")]
		ISpread<int> FKeyframeCount;
		
		[Output("Keyframe Value")]
		ISpread<double> FKeyframeValue;
		
		[Output("Keyframe Time")]
		ISpread<double> FKeyframeTime;
		
		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		
		#endregion fields & pins
		
		
		#region constructor/destructor
		public ValueTimelineParserNode()
		{
			
		}
		
		~ValueTimelineParserNode()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!FDisposed) {
				if (disposing) {
					// Dispose managed resources.
					//	FPatch.Added -= NodeAddedCB;
					//	FPatch.Removed -= NodeRemovedCB;
					
					//unregister all ioboxes
					//foreach (var node in FPatch)
					//if (node.ID == nodeID)
					//RemoveNodes(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor
		
		private void AddTracks()
		{
			foreach (var timeline in FPatch)
			{
				if (timeline.Name.Contains("IRIS.Timeline"))
				{
					foreach (var track in timeline)
					{
						if (track.Name.Contains("IRIS.Track") && !tracks.ContainsKey(track.ID.ToString()))
						{
							Track newTrack = new Track(track.Name,0);
							tracks.Add(track.ID.ToString(), newTrack);
						}
						
						foreach (var keyframe in track)
						{
							if (keyframe.Name.Contains("IRIS.Keyframe") && tracks.ContainsKey(track.ID.ToString()))
							{
								// parse time & value out of keyframe io boxes here
								
								Keyframe newKeyframe = new Keyframe(Convert.ToDouble(keyframe.FindPin("Time")[0].Trim('|')), Convert.ToDouble(keyframe.FindPin("Value")[0].Trim('|')),keyframe.ID.ToString() );
								//Keyframe newKeyframe = new Keyframe("hier","huhu");
								tracks[track.ID.ToString()].keyframes.Add(keyframe.ID.ToString(), newKeyframe);
							}
						}
					}
				}
			}
		}
		
		private void UpdateTimeline()
		{
			foreach (var timeline in FPatch)
			{
				if (timeline.Name.Contains("IRIS.Timeline"))
				{
					foreach (var track in timeline)
					{
						foreach (var currentTrack in tracks)
						{
							if (!tracks.ContainsKey(track.ID.ToString()))
							{
								Track newTrack = new Track(track.Name,0);
								tracks.Add(track.ID.ToString(), newTrack);
							}
							//else
								//tracks.Remove(track.ID.ToString());
						}
					}
				}
			}
		}
		
		private void UpdateTracks()
		{
			foreach (var timeline in FPatch)
			{
				if (timeline.Name.Contains("IRIS.Timeline"))
				{
					foreach (var track in timeline)
					{
						
					}
				}
			}
		}
		
		private void UpdateKeyframeValues(Keyframe kf)
		{
			foreach (var timeline in FPatch)
			{
				if (timeline.Name.Contains("IRIS.Timeline"))
				{
					foreach (var track in timeline)
					{
						
						foreach (var keyframe in track)
						{
							if (keyframe.ID.ToString() == kf.key)
							{
								// parse time & value out of keyframe io boxes here
								kf.val = Convert.ToDouble(keyframe.FindPin("Value")[0].Trim('|'));
								kf.time = Convert.ToDouble(keyframe.FindPin("Time")[0].Trim('|'));
								FLogger.Log (LogType.Debug, "hier");
							}
						}
					}
				}
			}
		}
		

		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			//update can be forced
				
				
			if (FInit[0] || (FUpdate[0] && firstFrame))
			{
				if (!firstFrame)
				tracks.Clear();
				
				nodePath = FNodePath[0];
				var node = FHDEHost.GetNodeFromPath(nodePath);
				FPatch = node.Parent;
				
				AddTracks();
				firstFrame=false;
			}
			
			//calc Output Slicecounts & write values into output-pins
			FTrackCount.SliceCount = 1;
			FKeyframeCount.SliceCount = tracks.Count;
			FTrackName.SliceCount = tracks.Count;
			
			FTrackCount[0] = tracks.Count;
			
			int maxKeyFrameCount = 0;
			int i = 0;
			int j = 0;
			if (FInit[0] || FUpdate[0])
			{
				 
			
				foreach (var track in tracks)
				{
				
					FKeyframeCount[i] = track.Value.keyframes.Count;
					FTrackName[i] = track.Value.name;
					
					
					maxKeyFrameCount +=  track.Value.keyframes.Count;
					FKeyframeValue.SliceCount = maxKeyFrameCount;
					FKeyframeTime.SliceCount = maxKeyFrameCount;
					
					
					foreach (var keyframe in track.Value.keyframes)
					{
						if (FInit[0] || FUpdate[0])
							UpdateKeyframeValues(keyframe.Value);
	
						FKeyframeValue[j] =  keyframe.Value.val;
						FKeyframeTime[j]= keyframe.Value.time;
						j++;
					}
					i++;	
				}
				
				//UpdateTimeline(); 
		}
			//FLogger.Log(LogType.Debug, "hi tty!");
		}
	}
	
	public class Track
	{
		public Dictionary<string, Keyframe> keyframes = new Dictionary<string, Keyframe>();
		public string name {get;set;}
		public int type {get;set;}
		
		public Track (string _name, int _type)
		{
			name = _name;
			type = _type;
		}
	}
	
	public class Keyframe
	{
		public double time {get;set;}
		public double val {get;set;}
		public string key {get;set;}
		public Keyframe (double _time, double _value, string _key)
		{
			time = _time;
			val = _value;
			key = _key;
		}
	}
}
