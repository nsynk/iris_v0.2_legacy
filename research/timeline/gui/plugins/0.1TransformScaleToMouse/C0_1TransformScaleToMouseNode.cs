#region usings
using System;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "ScaleToMouse", Category = "Transform", Help = "Scale Objects relative to mouse position", Tags = "matrix")]
	#endregion PluginInfo
	public class TransformScaleToMouseNode : IPluginEvaluate
	{
		#region fields & pins
		
		bool firstFrame = true;
		ISpread<Matrix4x4> temp;
		Vector2D currentInput;
		double xOffset;
		double yOffset;
		Matrix4x4 tmp;
		
		Vector2D lastFrame;
		
		[Input("Input", Visibility = PinVisibility.False)]
		ISpread<Matrix4x4> FInput;
		
		[Input("X")]
		ISpread<double> FX;
		[Input("Y")]
		ISpread<double> FY;
		
		[Input("Canvas Clamp Values")]
		ISpread<Vector4D> FClampValues;
		
		[Input("Clamp Canvas", IsSingle = true)]
		ISpread<bool> FClamp;
		
		[Input("ScaleFactor")]
		ISpread<double> FScaleFactor;
		
		[Input("Initialzie")]
		ISpread<bool> FInit;
		
		[Input("ScaleUp")]
		ISpread<bool> FScaleUp;
		
		[Input("ScaleMin")]
		ISpread<double> FScaleMin;
		
		[Input("ScaleMax")]
		ISpread<double> FScaleMax;
		
		[Input("ScaleDown")]
		ISpread<bool> FScaleDown;
		
		[Input("RightMouseButton")]
		ISpread<bool> FRightMouseButton;
		
		[Output("Output")]
		ISpread<Matrix4x4> FOutput;
		
		[Output("", Visibility = PinVisibility.False)]
		ISpread<Vector2D> LastFrameValue;
		

		[Import()]
		ILogger FLogger;
		#endregion fields & pins

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			currentInput.x = FX[0];
			currentInput.y = FY[0];
			xOffset = 0;
			yOffset=0;
			FOutput.SliceCount = SpreadMax;
			if(firstFrame){
			for (int i = 0; i < SpreadMax; i++){
					tmp = FInput[0];
					FOutput[i] = FInput[i];
				
				}
				temp = FInput;
				firstFrame=false;
			}
			if (FInit[0]){
				for (int i = 0; i < SpreadMax; i++){
					FInput[i] = VMath.Scale(1,1,1);
					FInput[i] = VMath.Translate(0,0,0);
					tmp = FInput[i];
					FOutput[i] = FInput[i];
					}
					
				}
			
			// calc Offset
			xOffset = currentInput.x-lastFrame.x;
			yOffset = currentInput.y-lastFrame.y;
			
			
			
		// Drag Canvas
			if (FRightMouseButton[0])
			{
				if (FClamp[0]){
					
					if ((FOutput[0].m41 <= FClampValues[0].x) || (FOutput[0].m41 >= FClampValues[0].y) ){
						
					}
					else
						tmp *= VMath.Translate(xOffset, yOffset, 0);
	
					
					/*
					if (xOffset >= FClampValues[0].y)
						xOffset = FClampValues[0].y;
					
					if (yOffset <= FClampValues[0].z)
						yOffset = FClampValues[0].z;
					if (yOffset >= FClampValues[0].w)
						yOffset = FClampValues[0].w;*/
					//tmp *= VMath.Translate(xOffset, yOffset, 0);
				}
				else{
					tmp *= VMath.Translate(xOffset, yOffset, 0);
				}
				
				
			}
			/*
			if (FRightMouseButton[0])
			{
				//check if canvas is at left border
				if ( FOutput[0].m41 >= 1000 )
				{
					tmp.m41 = 1000;
					if (xOffset <0)
						tmp *= VMath.Translate(xOffset, yOffset, 0);
					else
						tmp *= VMath.Translate(0, yOffset, 0);
				}
				else
					tmp *= VMath.Translate(xOffset, yOffset, 0);
	
			}*/
			
			
			

			
			if(FScaleDown[0] || FScaleUp[0]){
				for (int i = 0; i < SpreadMax; i++){
					tmp *= VMath.Translate(-FX[i], -FY[i], 0);
					if (FScaleDown[i] && FOutput[i].m33 > FScaleMin[i])
					tmp *= VMath.Scale(1-FScaleFactor[i], 1-FScaleFactor[i], 1-FScaleFactor[i]);
					if (FScaleUp[i] && FOutput[i].m33 < FScaleMax[i])
					tmp *= VMath.Scale(1/(1-FScaleFactor[i]), 1/(1-FScaleFactor[i]), 1/(1-FScaleFactor[i]));
					tmp *= VMath.Translate(FX[i], FY[i], 0);
				//	FOutput[i] = tmp;
				}
			}
		
		

			
			
			
			lastFrame = currentInput;
			LastFrameValue[0] = currentInput;
				FOutput[0] = tmp;
			
			
			
			
			
			
		}
		
		
		
		
	}
	
}

