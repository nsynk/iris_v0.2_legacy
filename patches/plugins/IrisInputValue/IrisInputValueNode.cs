#region usings
using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{



	#region PluginInfo
	[PluginInfo(Name = "InputValue", Category = "Iris", Help = "Basic Node to manage the Vertigo Interface along with the OSC-Messages", Tags = "Iris, v4Bundle, Parameter")]
	#endregion PluginInfo

	public class IrisInputValueNode : IPluginEvaluate
	{

		#region fields & pins
		string dstringORdXYZ = "";
		bool defaultOrOsc = true;
		bool IsString = false;
		bool hold = false;
		bool FListening = false;
		List<string> Helper = new List<string>();

		[Input("OSCMessages", DefaultString = "MyOSCMessage")]
		IDiffSpread<string> FOSCMessages;

		[Input("DefaultX", DefaultValue = 0.0)]
		IDiffSpread<double> FDefaultX;

		[Input("DefaultY", DefaultValue = 0.0)]
		IDiffSpread<double> FDefaultY;

		[Input("DefaultZ", DefaultValue = 0.0)]
		IDiffSpread<double> FDefaultZ;

		[Input("DefaultString", DefaultString = "defaultString")]
		IDiffSpread<string> FDefaultString;

		[Input("Controller Name", DefaultString = "Controller Name")]
		ISpread<string> FControllerName;

		[Input("UIType", EnumName = "UIType")]
		IDiffSpread<EnumEntry> FUIType;

		[Input("UIElementHeightFactor", DefaultValue = 1.0)]
		IDiffSpread<double> FUIElementHeightFactor;

		[Input("AllowMultipleSelection", IsSingle = true)]
		IDiffSpread<bool> FAllowMultipleSelection;

		[Input("EnumList (Comma separated)", DefaultString = "null")]
		IDiffSpread<string> FEnumList;

//-------------------------------------

		[Output("X")]
		ISpread<double> FX;

		[Output("Y")]
		ISpread<double> FY;

		[Output("Z")]
		ISpread<double> FZ;

		[Output("Strings")]
		ISpread<string> FStrings;

		[Output("Parameters")]
		ISpread<string> FControllerChannelsAndDefaults;

		[Output("OSCAddress")]
		ISpread<string> FOSCAddress;


		[Import()]
		IPluginHost FHost;
		[Import()]
		ILogger Flogger;

		#endregion fields & pins

		//Enum Constructor
		public IrisInputValueNode()
		{
			var enumz = new string[] {
				"X",
				"Y",
				"XY",
				"Bang",
				"Toggle",
				"Color(H+LasXYValue)",
				"VideoOrImage",
				"Image",
				"Video",
				"XFile",
				"Text",
				"LiveText",
				"AnyFile",
				"V4Patch",
				"V4Bundle",
				"Directory",
				"Enum"
			};
			EnumManager.UpdateEnum("UIType", enumz[0], enumz);
		}



		private OSCReceiver FOSCReceiver;


		private byte[] StringToByteArray(string str)
		{
			return System.Text.Encoding.GetEncoding(1252).GetBytes(str);
		}

		private void ListenToOSC()
		{
			for (int x = 0; x < FOSCMessages.SliceCount; x++) {
				if (FOSCMessages[x].Contains(FOSCAddress[0])) {
					OSCPacket packet = OSCPacket.Unpack(StringToByteArray(FOSCMessages[x]));
					{
						if (packet != null) {
							if (packet.IsBundle()) {
								ArrayList messages = packet.Values;
								for (int i = 0; i < messages.Count; i++) {
									ProcessOSCMessage((OSCMessage)messages[i]);
									//Flogger.Log(LogType.Debug, "packet is bundle");
								}
							} else {
								ProcessOSCMessage((OSCMessage)packet);
								//Flogger.Log(LogType.Debug, "packet isnt bundle");
							}

						} else
							Flogger.Log(LogType.Debug, "packet is not okay. either address mismatch or message is fucked up");
					}
				}
			}

		}
		private void ProcessOSCMessage(OSCMessage message)
		{
			string address = message.Address;
			ArrayList args = message.Values;
			Helper.Clear();
			if (address == FOSCAddress[0]) {
				defaultOrOsc = false;
				for (int i = 0; i < args.Count; i++)
					Helper.Add(Convert.ToString(args[i]));
			}
		}



		public void initialize()
		{
			//creating OSC-Address, depending on NodePath: ParentPatch(Container)/PatchName/ControllerName! e.g A1/halopoint/texture
			FHost.GetNodePath(true, out dstringORdXYZ);
			char[] splitchar = { '/' };
			string[] parts = dstringORdXYZ.Split(splitchar);
			dstringORdXYZ = parts[parts.Length - 2];
			FOSCAddress[0] = "/" + dstringORdXYZ+ "/" + FControllerName[0].Replace(" ", "_");
			dstringORdXYZ += "|" + FControllerName[0];
			dstringORdXYZ = dstringORdXYZ.Replace(" ", "_");

			//the controller specs are different if its a string or not
			if (FUIType[0].Name == "X" || FUIType[0].Name == "Y" || FUIType[0].Name == "XY" || FUIType[0].Name == "Bang" || FUIType[0].Name == "Toggle" || FUIType[0].Name == "Color(H+LasXYValue)") {
				IsString = false;
				dstringORdXYZ += "|" + Convert.ToString(FDefaultX[0]) + ";" + Convert.ToString(FDefaultY[0]) + ";" + Convert.ToString(FDefaultZ[0]);
				dstringORdXYZ=dstringORdXYZ.Replace(',', '.');		
			} else {
				IsString = true;
				dstringORdXYZ += "|";
				if (FAllowMultipleSelection[0])
				{
					for (int i=0; i<FDefaultString.SliceCount; i++ )
						{
							dstringORdXYZ += FDefaultString[i];
							if (i<FDefaultString.SliceCount-1)
								dstringORdXYZ += ";";
						}	
				}
				else
				dstringORdXYZ += FDefaultString[0];
			}
			dstringORdXYZ += "|" + FUIType[0].Name + "|" + Convert.ToString(FUIElementHeightFactor[0]).Replace(',', '.')	 + "|" + Convert.ToString(Convert.ToInt32(FAllowMultipleSelection[0])) + "|" + FEnumList[0];
			FControllerChannelsAndDefaults[0] = dstringORdXYZ;
		}
		public void Evaluate(int SpreadMax)
		{

			initialize();

			if (FDefaultX.IsChanged || FDefaultY.IsChanged || FDefaultZ.IsChanged || FDefaultString.IsChanged) {
				defaultOrOsc = true;
				FStrings.SliceCount=FDefaultString.SliceCount;
				for (int i = 0; i < FDefaultString.SliceCount; i++)
					FStrings[i] = FDefaultString[i];
				FX[0] = FDefaultX[0];
				FY[0] = FDefaultY[0];
				FZ[0] = FDefaultZ[0];
			}

			if (FOSCMessages.SliceCount > 0 && FOSCMessages.IsChanged) {
				ListenToOSC();
				if (IsString == false && defaultOrOsc == false) {

					//FStrings[0] = Convert.ToString(Convert.ToInt16(hold));
					if (Helper.Count > 1)
						FX[0] = double.Parse(Helper[0]);
					// System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
					if (Helper.Count > 2)
						FY[0] = double.Parse(Helper[1]);
					//, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
					if (Helper.Count > 3)
						FZ[0] = double.Parse(Helper[2]);
					//, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
				}
				if (IsString == true && defaultOrOsc == false) {
					FStrings.SliceCount = Helper.Count;
					//could be a spread
					for (int i = 0; i < Helper.Count; i++) {
						FStrings[i] = Helper[i];
					}
				}
			}
		}
	}
}
