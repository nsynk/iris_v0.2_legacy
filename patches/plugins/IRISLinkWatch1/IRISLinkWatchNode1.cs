#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;
using System.Drawing;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "LinkWatch", Category = "IRIS", Help = "", Tags = "", AutoEvaluate = true)]
	#endregion PluginInfo
	public class IRISLinkWatchNode1 : IDisposable, IPluginEvaluate
	{
		#region fields & pins
		[Input("NODE ID", IsSingle = true)]
		ISpread<string> FNodeID;

		[Input("NODE PATH", IsSingle = true)]
		ISpread<string> FNodePath;

		[Input("Update GUI", IsBang = true)]
		ISpread<bool> FUpdate;

		[Output("Output")]
		ISpread<string> FOutput;

		[Import()]
		ILogger FLogger;

		[Import()]
		IHDEHost FHDEHost;

		[Import()]
		IPluginHost FPluginHost;

		int nodeID;
		private INode2 FPatch;
		private bool FFirstFrame = true;
		private Dictionary<string, INode2> FNodes = new Dictionary<string, INode2>();
		private string debug;
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		#endregion fields & pins

		string nodePath;
		string handleOut;
		#region constructor/destructor
		public IRISLinkWatchNode1()
		{
		}

		~IRISLinkWatchNode1()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!FDisposed) {
				if (disposing) {
					// Dispose managed resources.
					FPatch.Added -= NodeAddedCB;
					FPatch.Removed -= NodeRemovedCB;

					//unregister all ioboxes
					foreach (var node in FPatch)
						if (node.ID == nodeID)
							RemoveNodes(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor

		#region events
		//when a node is removed from the patch 
		//and it is one of the observed subpatches
		//then unregister all parameter-pins
		//reregister all parameter-pins

		private void NodeAddedCB(IViewableCollection<INode2> collection, INode2 node)
		{
			if (node.ID == nodeID)
				AddNodes(node);

			FOutput.AssignFrom(FNodes.Keys);
		}

		private void NodeRemovedCB(IViewableCollection<INode2> collection, INode2 node)
		{
			RemoveNodes(node);

			FOutput.AssignFrom(FNodes.Keys);
		}

		private void InputChangedCB(object sender, EventArgs e)
		{
			var patch = (sender as IPin2).ParentNodeByPatch(FPatch);
			RemoveNodes(patch);

//			FLogger.Log(LogType.Debug, patch.Name);

			AddNodes(patch);
			FOutput.AssignFrom(FNodes.Keys);
		}
		#endregion events

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FFirstFrame) {

				//FPluginHost.GetNodePath(false, out nodePath);
				nodePath = FNodePath[0];
				var node = FHDEHost.GetNodeFromPath(nodePath);
				FPatch = node.Parent;

				//register at this nodes patch to be informed of added/removed nodes
				//FPatch.Added += NodeAddedCB;
				//FPatch.Removed += NodeRemovedCB;

				UpdateAllInputs();
				FOutput.AssignFrom(FNodes.Keys);
				FFirstFrame = false;

				if (FNodeID[0].Length == 0)
					nodeID = 0;
				else
					nodeID = Convert.ToInt32(FNodeID[0]);
			}

			//update can be forced
			if (FUpdate[0]) {
				FNodes.Clear();

				if (FNodeID[0].Length == 0)
					nodeID = 0;
				else
					nodeID = Convert.ToInt32(FNodeID[0]);

				UpdateAllInputs();
				FOutput.AssignFrom(FNodes.Keys);

			}


		}

		private void UpdateAllInputs()
		{
			//go through all nodes in this patch
			//if it is a node from a given path
			//extract its inputs
			foreach (var node in FPatch) {
				//if (node.NodeInfo.Filename.Contains(FFXPath[0]))
				if (node.ID == nodeID)
					AddNodes(node);

			}
		}



		private void AddNodes(INode2 patch)
		{
			string connection = "";
			string srcpinname = "";
			var handleID = "0";
			var desc ="";
			/*
				//for all nodes in the patch
				foreach (var node in patch) {

					if (node.Name == "Info (EX9.Texture)" && node.IsConnected()) {
						var output = node.FindPin("Texture");
						foreach (var pin in output.ConnectedPins) {
							var handle = node.FindPin("Shared Handle");
							var format = node.FindPin("Format");
							handleOut += "|" + pin.ParentNode.Parent.ID + ":" + handle[0] + "|" + format[0];
						}
					}
					debug = handleID;
				}
			
			*/
			foreach (var node in patch) {

				if (node.NodeInfo.Filename.Contains(".v4p")) {
					var param = "<LINK srcnodeid='" + node.ID.ToString() + "'";
					
					if (node.Name.Contains("EX9.Texture")) {
						
						var output = node.FindPin("Output");
						if (output.CLRType == null) {
							
							foreach (var pin in output.ConnectedPins) {
								//desc = pin.FindPin("Descriptive Name").Spread.ToString();
								if (pin.ParentNode.Parent.Name.Contains("EX9")){
									connection = pin.ParentNode.Parent.ID.ToString();
								//	srcpinname = pin.
								}
							}

						}
					} 
					else
						connection = "-1";



					param += " srcpinname='" + desc + "' " + connection + "|";
					FNodes.Add(param, node);
				}
				}



			
		}


		
		private string GetSrcPinName(INode2 node)
		{
			string name = "";
			var output = node.FindPin("Output");
			if (output.CLRType == null) {
							foreach (var pin in output.ConnectedPins) {
								if (pin.ParentNode.Parent.Name.Contains("EX9")){
									name = pin.ParentNode.Parent.ID.ToString();
									
								}
							}

						}
			return name;
		}
		







		private void RemoveNodes(INode2 patch)
		{
			//remove all inputs of that node
			var nodePath = patch.GetNodePath(false);
			var keysToDelete = new List<string>();
			foreach (var key in FNodes.Keys)
				if (key.StartsWith(nodePath))
					keysToDelete.Add(key);

			foreach (var key in keysToDelete) {
				var node = FNodes[key];
				FNodes.Remove(key);
			}

		}

		
	}
}
