#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;
using System.Drawing;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "GUIExporter", 
				Category = "IRIS", 
				Help = "", Tags = "",
				AutoEvaluate = true)]
	#endregion PluginInfo
	public class NetworkGUIServerNode: IDisposable, IPluginEvaluate
	{
		#region fields & pins
		[Input("FX Path", IsSingle = true)]
		ISpread<string>FFXPath;
		
		[Input("NODE PATH", IsSingle = true)]
		ISpread<string> FNodePath;
		
		[Input("Update GUI", IsBang = true)]
		ISpread<bool> FUpdate;

		[Output("Output")]
		ISpread<string> FOutput;

		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		
		private INode2 FPatch;
		private bool FFirstFrame = true;
		private Dictionary<string, INode2> FInputs = new Dictionary<string, INode2>();
		
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		#endregion fields & pins
		
		#region constructor/destructor
		public NetworkGUIServerNode()
		{}
		
		~NetworkGUIServerNode()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!FDisposed)
			{
				if(disposing)
				{
					// Dispose managed resources.
					FPatch.Added -= NodeAddedCB;
					FPatch.Removed -= NodeRemovedCB;
					
					//unregister all ioboxes
					foreach (var node in FPatch)
						if (node.NodeInfo.Filename.Contains(FFXPath[0]))
							RemoveInputs(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor
		
		#region events
		//when a node is removed from the patch 
		//and it is one of the observed subpatches
		//then unregister all parameter-pins
		//reregister all parameter-pins
		
		private void NodeAddedCB(IViewableCollection<INode2> collection, INode2 node)
		{
//			if (node.NodeInfo.Filename.Contains(FFXPath[0]))
//				AddInputs(node);
//			
//			FOutput.AssignFrom(FInputs.Keys);
		}
		
		private void NodeRemovedCB(IViewableCollection<INode2> collection, INode2 node)
		{
//			RemoveInputs(node);
//			
//			FOutput.AssignFrom(FInputs.Keys);
		}
		
		private void InputChangedCB(object sender, EventArgs e)
		{
//			var patch = (sender as IPin2).ParentNodeByPatch(FPatch);
//			RemoveInputs(patch);
//			
////			FLogger.Log(LogType.Debug, patch.Name);
//			
//			AddInputs(patch);
//			FOutput.AssignFrom(FInputs.Keys);
		}
		#endregion events
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FFirstFrame || FUpdate[0])
			{
				string nodePath;
				//FPluginHost.GetNodePath(false, out nodePath);
				nodePath = FNodePath[0];
				var node = FHDEHost.GetNodeFromPath(nodePath);
				//FPatch = node.Parent;
				if (node != null)
				{
					FPatch = node;
					
					//register at this nodes patch to be informed of added/removed nodes
					FPatch.Added += NodeAddedCB;
					FPatch.Removed += NodeRemovedCB;
					FInputs.Clear();
					UpdateAllInputs();
					FOutput.AssignFrom(FInputs.Keys);
					FFirstFrame = false;
				
				}
			}
		}
		
		private void UpdateAllInputs()
		{
			//go through all nodes in this patch
			//if it is a node from a given path
			//extract its inputs
			
			foreach (var node in FPatch)
				if (node.NodeInfo.Filename.Contains(FFXPath[0]))
					AddInputs(node);
				
			
		}
		
		private void AddInputs(INode2 patch)
		{
			//for all nodes in the patch
			foreach (var node in patch)
			{
				//bool isSignal = false;
			
				
				//check for ioboxes that are inputs to the patch
				//&& (node.LabelPin.Spread != "Evaluate")
				if ((!FInputs.ContainsValue(node))
					&& (node.NodeInfo.Name == "IOBox")
					&& (node.NodeInfo.Category != "Node")
					&& (node.LabelPin.Spread != "||"))
				{
					var inputName = GetIOBoxPinName(node.NodeInfo.Category, true); 
					var outputName = GetIOBoxPinName(node.NodeInfo.Category, false); 
					
					var input = node.FindPin(inputName);
					var output = node.FindPin(outputName);
					
	
					if ((!input.IsConnected() && output.IsConnected())  ||  (!output.IsConnected() && !input.IsConnected() && node.FindPin("Behavior").Spread=="Bang")  )
					{
						var param = node.GetNodePath(false) + "/" + inputName;
						
						node.LabelPin.Changed += InputChangedCB;
						var tag = node.FindPin("Tag");
						tag.Changed += InputChangedCB;
						input.SubtypeChanged += InputChangedCB;
						
						param += "\"" + GetSubtypeFromIOBox(node);
						param = "<PIN pinPath=\"" + param;
						FInputs.Add(param, node);
					}
					
					else if ( (!output.IsConnected()&& input.IsConnected()))
					{
						FLogger.Log(LogType.Debug, ("Output Pin Name " + node.ToString() + output.ToString())); 
						var param = node.GetNodePath(false) + "/" + inputName;
						
						node.LabelPin.Changed += InputChangedCB;
						var tag = node.FindPin("Tag");
						tag.Changed += InputChangedCB;
						input.SubtypeChanged += InputChangedCB;
						
						param += "\"" + GetSubtypeFromIOBox(node);
						param = "<PIN pinPath=\"" + param;
						FInputs.Add(param, node); 
					}
				}
			}
		}
		
		private void RemoveInputs(INode2 patch)
		{
			//remove all inputs of that node
			var nodePath = patch.GetNodePath(false);
			var keysToDelete = new List<string>();
			foreach (var key in FInputs.Keys)
				if (key.StartsWith(nodePath))
					keysToDelete.Add(key); 
			
			foreach (var key in keysToDelete)
			{
				var node = FInputs[key];
				node.LabelPin.Changed -= InputChangedCB;
				var tag = node.FindPin("Tag");
				tag.Changed -= InputChangedCB;
				var input = node.FindPin(GetIOBoxPinName(node.NodeInfo.Category, true));
				//var output = node.FindPin(GetIOBoxPinName(node.NodeInfo.Category, false));
				input.SubtypeChanged -= InputChangedCB;
				//output.SubtypeChanged -= InputChangedCB;
				
				FInputs.Remove(key);
			}
		}
		
		private string GetSubtypeFromIOBox(INode2 iobox)
		{
			var category = iobox.NodeInfo.Category;
			var pinLabel = iobox.LabelPin.Spread.Trim('|');//.Spread.Trim('|');
			var subType ="" ;
			var sliderBehavior = "";
			var behavior = "";
			var vectorSize = "";
			var valueType = "";
			var tag = "";
			var defaultValue = "";
			var guiHeight = "1";
			var sliceCountMode = "";
			var sliceCount = "";
			var minValue = "";
			var maxValue = "";
			var rows = "";
			var fileMask = "";
			var allowMultiple = "0";
			var enums = "null";
			var vvvvValueType = "";
			
			// IO-BOX VALUE
			if (category == "Value")
			{
				//set Value Type
				valueType = "value";
				
				// set Subtype
				sliderBehavior = iobox.FindPin("Slider Behavior").Spread;
				behavior = iobox.FindPin("Behavior").Spread;
				vvvvValueType = iobox.FindPin("Value Type").Spread;
				if (sliderBehavior == "" && behavior == "")
					sliderBehavior = "X";		
				if (sliderBehavior == "Toggle" || behavior == "Toggle" )
					subType = "toggle";
				if (sliderBehavior == "Bang"  || behavior == "Bang")
					subType = "bang";
				if ((sliderBehavior == "Endless" && vvvvValueType != "Boolean" )|| (sliderBehavior == "Slider" && vvvvValueType != "Boolean")) {
					vectorSize = iobox.FindPin("Vector Size").Spread;
					if (vectorSize == "1")
						subType = "xSlider";
					else if (vectorSize == "2")
						subType = "xySlider";
					else if (vectorSize == "3" )
						subType = "xyzSlider";
					else
						subType = "xyzSlider";
				}
				//set Tags
				tag = iobox.FindPin("Tag").Spread;
				if (tag =="||")
					tag="";
				// set default Value
				defaultValue = iobox.FindPin("Y Input Value").Spread;
				//set Gui Height
				guiHeight = "300"; //iobox.GetBounds(BoundsType.Box).Height.ToString();
				if (subType == "xySlider" || subType == "xyzSlider")
					guiHeight = "900";
				// set Allow Multiple
				allowMultiple = "0";
				// set Min/Max Values
				if (vvvvValueType == "Boolean"){
					minValue = "0";
					maxValue = "1";
				}
				else{
					minValue = iobox.FindPin("Minimum").Spread;
					maxValue = iobox.FindPin("Maximum").Spread;
				}
			}
			// IO-BOX STRING
			else if (category == "String")
			{
				//set ValueType
				valueType = "string";
				
				// set Subtype via FileMask
				fileMask = iobox.FindPin("File Mask").Spread.Trim('|');
				if (fileMask == "")
					subType = "textInput";
				if (fileMask == "Enum" || fileMask == "enum" || fileMask == "eNum" || fileMask == "ENUM")
					subType = "enum";
				else
					subType = fileMask;
				
				//set Tag
				//if (subType == "enum")
					tag = iobox.FindPin("Tag").Spread.Trim('|');

				//else tag = "";
	
				//set DefaultValue
				defaultValue = iobox.FindPin("Input String").Spread.Trim('|');	
				if (defaultValue == "")
					defaultValue = "nil";
				
				//set Gui Height
				guiHeight = "300"; //iobox.GetBounds(BoundsType.Box).Height.ToString();
				
				//set AllowMultiple
				sliceCountMode = iobox.FindPin("SliceCount Mode").Spread;
				rows = iobox.FindPin("Rows").Spread;
				if (sliceCountMode == "Input")
					allowMultiple = "0"; // unlimited
				else
					allowMultiple = rows.ToString();
				
				// set Min/Max Values
				minValue="0";
				maxValue="1";
				
			}
			
			// IO-BOX COLOR
			else if (category == "Color")
			{
				valueType = "color";
				subType = iobox.FindPin("Chooser Style").Spread;
				
				//set Tags
				tag = iobox.FindPin("Tag").Spread;
				if (tag =="||")
					tag="";
				
				//set DefaultValue
				defaultValue = iobox.FindPin("Color Input").Spread.Trim('|');
				//FLogger.Log(LogType.Debug, defaultValue);
				
				//set Gui Height
				guiHeight = "900"; //iobox.GetBounds(BoundsType.Box).Height.ToString();
				
				// set Allow Multiple
				allowMultiple = "0";
				
				// set Min/Max Values
				minValue="0";
				maxValue="1";
			}

			// IO-BOX ENUM
			else if (category == "Enumerations")
			{
				valueType = "string";
				subType = "enum";
				
				var IOEnum = iobox.FindPin("Input Enum").SubType.ToString();
				IOEnum = IOEnum.Split(',')[1].Trim();
				var enumCount =  FHDEHost.GetEnumEntryCount(IOEnum);
				var allEnums = "";

				 for (int i=0; i < enumCount; i++){
					allEnums += FHDEHost.GetEnumEntry(IOEnum, i) + ", ";
				}
				
				tag = allEnums; //iobox.FindPin("Tag").Spread.Trim('|');
				guiHeight = "300"; //iobox.GetBounds(BoundsType.Box).Height.ToString();
				allowMultiple = "1";
			}
			
			// Setting Type to Signal for upstream connected ioboxes & to cue for not connected ioboxes
			var inputName = GetIOBoxPinName(iobox.NodeInfo.Category, true); 	
			var input = iobox.FindPin(inputName);

			var outputName = GetIOBoxPinName(iobox.NodeInfo.Category, false);
			var output = iobox.FindPin(outputName);
			if ( (input.IsConnected() &&  !output.IsConnected()) ) 
					{
						
						valueType = "signal";
					}
			
			
			if ( !input.IsConnected() &&  !output.IsConnected() ) 		{
						valueType = "cue";
					}
			
			return  " pinLable=\"" + pinLabel + "\" valueType=\"" + valueType + "\" valueSubType=\"" + subType + "\" tag=\"" + tag + "\" defaultValue=\"" + defaultValue + "\" guiHeight=\"" + guiHeight + "\" allowMultiple=\"" + allowMultiple + "\" minValue=\"" + minValue + "\" maxValue=\"" + maxValue +"\" > </PIN>";
		}

		private string GetIOBoxPinName(string pinType, bool input)
		{
			if (pinType == "String")
			{	if (input)
					return "Input String";
				else
					return "Output String";}
			else if (pinType == "Value")
			{	if (input)
					return "Y Input Value";
				else
					return "Y Output Value";}
			else if (pinType == "Color")
			{	if (input)
					return "Color Input";
				else
					return "Color Output";}
			else if (pinType == "Enumerations")
			{	if (input)
					return "Input Enum";
				else
					return "Output Enum";}
			else //assume node
			{	
				if (input)
					return "Input Node";
				else
					return "Output Node";
			}
		}
	}
}
