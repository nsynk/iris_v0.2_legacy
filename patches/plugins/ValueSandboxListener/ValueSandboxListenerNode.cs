#region usings
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Drawing;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	public class Listener
	{
		public readonly ObservableDictionary<string, INode2> FNodes = new ObservableDictionary<string, INode2>();
	}
	
	#region Listener
	#region PluginInfo
	[PluginInfo(Name = "SandboxListener", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class ValueSandboxListenerNode : IDisposable, IPluginEvaluate
	{
		#region fields & pins
		//Fields
		private Listener listener;
		private string nodePath;
		private VVVV.PluginInterfaces.V2.Graph.INode2 FSandbox;
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		private INode2 FPatch;
		private bool FFirstFrame = true;
		
		//Pins
		[Input("Node Path", IsSingle = true)]
		IDiffSpread<string> FNodePath;
		
		[Output("Sandbox")]
		ISpread<Listener> FDict;
		
		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		#endregion fields & pins
		
		#region constructor/destructor
		public ValueSandboxListenerNode()
		{
			listener = new Listener();
		}
		
		~ValueSandboxListenerNode()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!FDisposed)
			{
				if(disposing)
				{
					// Dispose managed resources.
					FPatch.Added -= NodeAddedCB;
					FPatch.Removed -= NodeRemovedCB;
					
					//unregister all ioboxes
					foreach (var node in FPatch)
					if (node.NodeInfo.Filename.Contains("v4p"))
					RemoveNodes(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor
		
		
		#region events
		//when a node is removed from the patch
		//and it is one of the observed subpatches
		//then unregister all parameter-pins
		//reregister all parameter-pins
		
		private void NodeAddedCB(IViewableCollection<INode2> collection, INode2 node)
		{
			AddNodes(node);
		}
		
		private void NodeRemovedCB(IViewableCollection<INode2> collection, INode2 node)
		{
			RemoveNodes(node);
		}
		
		private void InputChangedCB(object sender, EventArgs e)
		{
			var patch = (sender as IPin2).ParentNodeByPatch(FPatch);
			//			RemoveInputs(patch);
			//
			FLogger.Log(LogType.Debug, "HIER" );
			//
			//			AddInputs(patch);
			//			FOutput.AssignFrom(FInputs.Keys);
		}
		#endregion events
		
		
		
		private void AddNodes(INode2 patch)
		{
			if (!listener.FNodes.ContainsKey(patch.ID.ToString()) &&  patch.NodeInfo.Filename.Contains("v4p"))
			{
				listener.FNodes.Add(patch.ID.ToString(), patch);
				
				
				
				
				//input.LabelPin.Changed += InputChangedCB;
				
				//	input.LabelPin.Changed += InputChangedCB;
				
			}
		}
		
		private void RemoveNodes(INode2 patch)
		{
			if (listener.FNodes.ContainsKey(patch.ID.ToString()))
			{
				listener.FNodes.Remove(patch.ID.ToString());
			}
		}
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			
			if (FNodePath.IsChanged)
			{
				nodePath = FNodePath[0];
				FSandbox = FHDEHost.GetNodeFromPath(nodePath);
				
				FSandbox.Added += NodeAddedCB;
				FSandbox.Removed += NodeRemovedCB;
			}
			
			if (FFirstFrame)
			{
				FDict.SliceCount=0;
				nodePath = FNodePath[0];
				//listener.NodeChanged += InputChangedCB;
				FSandbox = FHDEHost.GetNodeFromPath(nodePath);
				foreach (var patch in FSandbox)
				{
					AddNodes(patch);
				}
				FDict.Add(listener);
				FFirstFrame=false;
			}
			
		}
	}
	#endregion Listener
	
	#region GetID
	#region PluginInfo
	[PluginInfo(Name = "GetID", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class GetID : Listener, IPluginEvaluate
	{
		[Input("Sandbox")]
		ISpread<Listener> FDict;
		
		[Output("ID")]
		ISpread<string> FIDOut;
		
		[Import()]
		ILogger FLogger;
		
		bool FFirstFrame = true;
		bool FUpdate = false;
		
		public void Evaluate(int SpreadMax)
		{
			
			if (FDict[0]!=null && FFirstFrame)
			{
				FIDOut.SliceCount = FDict[0].FNodes.Count;
				FIDOut.AssignFrom(FDict[0].FNodes.Keys);
			}
			if(FFirstFrame){
				FDict[0].FNodes.CollectionAltered += new CollectionAlteredEventHander(DictionaryChanged);
				FFirstFrame=false;
			}
			if (FUpdate){
				FIDOut.AssignFrom(FDict[0].FNodes.Keys);
				FUpdate=false;
			}
		}
		
		private void DictionaryChanged(object sender, EventArgs e)
		{
			if (FDict[0]!=null)
			{
				FUpdate=true;
			}
		}
	}
	#endregion GetID
	
	#region GetName
	#region PluginInfo
	[PluginInfo(Name = "GetName", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class GetName : Listener, IPluginEvaluate
	{
		[Input("Sandbox")]
		ISpread<Listener> FDict;
		
		[Output("Name")]
		ISpread<string> FNameOut;
		
		[Import()]
		ILogger FLogger;
		
		bool FFirstFrame = true;
		bool FUpdate = false;
		
		public void Evaluate(int SpreadMax)
		{
			if(FFirstFrame)
			FDict[0].FNodes.CollectionAltered += new CollectionAlteredEventHander(DictionaryChanged);
			
			if ((FUpdate || FFirstFrame) &&  FDict[0]!=null)
			{
				
				FNameOut.SliceCount = FDict[0].FNodes.Count;
				int i=0;
				foreach (var node in FDict[0].FNodes)
				{
					FNameOut[i] = node.Value.Name;
					i++;
				}
				FUpdate=false;
				FFirstFrame = false;
			}
			
		}
		
		private void DictionaryChanged(object sender, EventArgs e)
		{
			if (FDict[0]!=null)
			{
				FUpdate=true;
			}
		}
	}
	#endregion GetName
	
	#region GetTextureInput
	#region PluginInfo
	[PluginInfo(Name = "GetTextureInput", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class GetTextureInput : Listener, IPluginEvaluate
	{
		[Input("Sandbox")]
		ISpread<Listener> FDict;
		
		[Output("Bin Size")]
		ISpread<int> FBinSizeOut;
		
		[Output("Pin Name")]
		ISpread<string> FPinNameOut;
		
		[Import()]
		ILogger FLogger;
		
		bool FFirstFrame = true;
		bool FUpdate = false;
		List<string> pins = new List<string>();
		
		public void Evaluate(int SpreadMax)
		{
			if(FFirstFrame)
			FDict[0].FNodes.CollectionAltered += new CollectionAlteredEventHander(DictionaryChanged);
			
			if ((FUpdate || FFirstFrame) &&  FDict[0]!=null)
			{
				pins.Clear();
				FBinSizeOut.SliceCount = FDict[0].FNodes.Count;
				
				int c=0;
				foreach (var node in FDict[0].FNodes)
				{
					int tmp = GetPins(node.Value);
					FBinSizeOut[c] = tmp;
					
					c++;
				}
				FPinNameOut.SliceCount=pins.Count;
				FPinNameOut.AssignFrom(pins);
				
				FUpdate=false;
				FFirstFrame = false;
			}
		}
		
		private void DictionaryChanged(object sender, EventArgs e)
		{
			if (FDict[0]!=null)
			{
				FUpdate=true;
			}
		}
		
		private int GetPins(INode2 patch)
		{
			int tmp =0;
			
			foreach (var node in patch){
				var input = node.FindPin("Input Node");
				var output = node.FindPin("Output Node");
				
				if (node.NodeInfo.Name == "IOBox" && node.NodeInfo.Category == "Node" && patch.Name.Contains("EX9") && node.LabelPin.Spread.Trim('|') != "" )
				{
					foreach (var cPin in output.ConnectedPins)
					{
						if ((cPin.ParentNode.Parent.GetNodePath(true).Contains(patch.GetNodePath(true))))
						{
							tmp++;
							pins.Add(GetPinLable(node));
						}
						break;
					}
				}
			}
			return tmp;
		}
		
		private string GetPinLable(INode2 iobox)
		{
			return iobox.LabelPin.Spread.Trim('|');
		}
	}
	#endregion GetTextureInput
	
	#region GetTextureOutput
	#region PluginInfo
	[PluginInfo(Name = "GetTextureOutput", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class GetTextureOutput : Listener, IPluginEvaluate
	{
		[Input("Sandbox")]
		ISpread<Listener> FDict;
		
		[Output("Bin Size")]
		ISpread<int> FBinSizeOut;
		
		[Output("Pin Name")]
		ISpread<string> FPinNameOut;
		
		[Import()]
		ILogger FLogger;
		
		bool FFirstFrame = true;
		bool FUpdate = false;
		List<string> pins = new List<string>();
		
		public void Evaluate(int SpreadMax)
		{
			if(FFirstFrame)
			FDict[0].FNodes.CollectionAltered += new CollectionAlteredEventHander(DictionaryChanged);
			
			if ((FUpdate || FFirstFrame) &&  FDict[0]!=null)
			{
				pins.Clear();
				FBinSizeOut.SliceCount = FDict[0].FNodes.Count;
				
				int c=0;
				foreach (var node in FDict[0].FNodes)
				{
					int tmp = GetPins(node.Value);
					FBinSizeOut[c] = tmp;
					
					c++;
				}
				FPinNameOut.SliceCount=pins.Count;
				FPinNameOut.AssignFrom(pins);
				
				FUpdate=false;
				FFirstFrame = false;
			}
		}
		
		private void DictionaryChanged(object sender, EventArgs e)
		{
			if (FDict[0]!=null)
			{
				FUpdate=true;
			}
		}
		
		private int GetPins(INode2 patch)
		{
			int tmp =0;
			
			foreach (var node in patch){
				var input = node.FindPin("Input Node");
				var output = node.FindPin("Output Node");
				
				if (node.NodeInfo.Name == "IOBox" && node.NodeInfo.Category == "Node" && patch.Name.Contains("EX9") && node.LabelPin.Spread.Trim('|') != "" )
				{
					foreach (var cPin in input.ConnectedPins)
					{
						if ((cPin.ParentNode.Parent.GetNodePath(true).Contains(patch.GetNodePath(true))))
						{
							tmp++;
							pins.Add(GetPinLable(node));
						}
						//break;
					}
				}
			}
			return tmp;
		}
		
		private string GetPinLable(INode2 iobox)
		{
			return iobox.LabelPin.Spread.Trim('|');
		}
	}
	#endregion GetTextureOutput
	
	#region GetPos
	#region PluginInfo
	[PluginInfo(Name = "GetNodePos", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class GetPos : Listener, IPluginEvaluate
	{
		[Input("Sandbox")]
		ISpread<Listener> FDict;
		
		[Output("Postion")]
		ISpread<Vector4D> FPosOut;
		
		[Output("Bounds")]
		ISpread<Vector4D> FBoundsOut;
		
		bool FFirstFrame = true;
		bool FUpdate = false;
		
		public void Evaluate(int SpreadMax)
		{
			if(FFirstFrame)
			FDict[0].FNodes.CollectionAltered += new CollectionAlteredEventHander(DictionaryChanged);
			
			if ((FUpdate || FFirstFrame) &&  FDict[0]!=null)
			{
				FBoundsOut.SliceCount = 1;
				FPosOut.SliceCount = FDict[0].FNodes.Count;
				
				int xMin=100000000;
				int xMax=0;
				int yMin=100000000;
				int yMax=0;
				
				int i=0;
				foreach (var node in FDict[0].FNodes)
				{
					Vector4D tmp = GetNodePos(node.Value);
					FPosOut[i] = GetNodePos(node.Value);
					/*
if(tmp.x<xMin)
xMin=(int)tmp.x;
if((tmp.x+tmp.y)>xMax)
xMax= (int)(tmp.x+tmp.y);*/
					i++;
				}
				//	FBoundsOut[0]= new Vector4D(xMin, xMax, 0 , 0);
				FUpdate=false;
				FFirstFrame = false;
			}
		}
		
		private void DictionaryChanged(object sender, EventArgs e)
		{
			if (FDict[0]!=null)
			{
				FUpdate=true;
			}
		}
		
		private Vector4D GetNodePos(INode2 node)
		{
			// LEFT, WIDTH, TOP, HEIGHT
			return new Vector4D(node.GetBounds(BoundsType.Node).Left, node.GetBounds(BoundsType.Node).Width, node.GetBounds(BoundsType.Node).Top, node.GetBounds(BoundsType.Node).Height);
		}
	}
	#endregion GetPos
	
	#region GetParameters
	#region PluginInfo
	[PluginInfo(Name = "GetParameters", Category = "IRIS Sandbox Listener", Help = "", Tags = "")]
	#endregion PluginInfo
	public class GetParameters : Listener, IPluginEvaluate
	{
		[Input("Sandbox")]
		ISpread<Listener> FDict;
		
		[Output("Bin Size")]
		ISpread<int> FBinSizeOut;
		
		[Output("Pin Name")]
		ISpread<string> FPinNameOut;
		
		[Output("Gui Type")]
		ISpread<string> FGuiTypeOut;
		
		[Import()]
		ILogger FLogger;
		
		bool FFirstFrame = true;
		bool FUpdate = false;
		List<string> FPinNames = new List<string>();
		List<string> FGuiTypes = new List<string>();
		
		public void Evaluate(int SpreadMax)
		{
			if(FFirstFrame)
			FDict[0].FNodes.CollectionAltered += new CollectionAlteredEventHander(DictionaryChanged);
			
			if ((FUpdate || FFirstFrame) &&  FDict[0]!=null)
			{
				FPinNames.Clear();
				FGuiTypes.Clear();
				FBinSizeOut.SliceCount = FDict[0].FNodes.Count;
				
				int c=0;
				foreach (var node in FDict[0].FNodes)
				{
					int tmp = GetParameterCount(node.Value);
					FBinSizeOut[c] = tmp;
					c++;
				}
				FPinNameOut.SliceCount=FPinNames.Count;
				FGuiTypeOut.SliceCount=FPinNames.Count;
				FPinNameOut.AssignFrom(FPinNames);
				FGuiTypeOut.AssignFrom(FGuiTypes);
				
				FUpdate=false;
				FFirstFrame = false;
			}
		}
		
		int GetParameterCount(INode2 patch)
		{
			int i=0;
			foreach (var node in patch)
			{
				if(IsValidParameter(node))
					i++;
			}
			return i;
		}
		
		bool IsValidParameter (INode2 node)
		{
			if ((node.NodeInfo.Name == "IOBox")
			&& (node.NodeInfo.Category != "Node")
			&& (node.LabelPin.Spread != "||"))
			{
				var inputName = GetIOBoxPinName(node.NodeInfo.Category, true);
				var outputName = GetIOBoxPinName(node.NodeInfo.Category, false);
				
				var input = node.FindPin(inputName);
				var output = node.FindPin(outputName);
				
				if (input != null && output != null)
				{
					if ((!input.IsConnected() && output.IsConnected())  ||  (!output.IsConnected() && !input.IsConnected() && node.FindPin("Behavior").Spread=="Bang"))
					{
						FPinNames.Add(GetPinLable(node));
						FGuiTypes.Add(GetSubType(node, GetCategory(node)));
						return true;
					}
				}
			}
			return false;
		}
		
		string GetIOBoxPinName(string pinType, bool input)
		{
			if (pinType == "String")
			{	if (input)
				return "Input String";
				else
				return "Output String";}
			else if (pinType == "Value")
			{	if (input)
				return "Y Input Value";
				else
				return "Y Output Value";}
			else if (pinType == "Color")
			{	if (input)
				return "Color Input";
				else
				return "Color Output";}
			else if (pinType == "Enumerations")
			{	if (input)
				return "Input Enum";
				else
				return "Output Enum";}
			else //assume node
			{
				if (input)
				return "Input Node";
				else
				return "Output Node";
			}
		}
		
		string GetCategory (INode2 IoBox)
		{
			return IoBox.NodeInfo.Category;
		}
		string GetPinLable (INode2 IoBox)
		{
			return IoBox.LabelPin.Spread.Trim('|');
		}
		string GetSubType (INode2 IoBox, string category)
		{
			var sliderBehavior = IoBox.FindPin("Slider Behavior").Spread;
			var behavior = IoBox.FindPin("Behavior").Spread;
			var vvvvValueType = IoBox.FindPin("Value Type").Spread;
			var valueType = "";
			var subType = "";
			var vectorSize ="";
			var tag = "";
			var defaultValue ="";
			var guiHeight ="";
			var allowMultiple ="";
			var minValue ="";
			var maxValue ="";
			
			if (category == "Value")
			{
				//set Value Type
				valueType = "value";
				
				// set Subtype
				sliderBehavior = IoBox.FindPin("Slider Behavior").Spread;
				behavior = IoBox.FindPin("Behavior").Spread;
				vvvvValueType = IoBox.FindPin("Value Type").Spread;
				if (sliderBehavior == "" && behavior == "")
					sliderBehavior = "X";		
				if (sliderBehavior == "Toggle" || behavior == "Toggle" )
					subType = "toggle";
				if (sliderBehavior == "Bang"  || behavior == "Bang")
					subType = "bang";
				if ((sliderBehavior == "Endless" && vvvvValueType != "Boolean" )|| (sliderBehavior == "Slider" && vvvvValueType != "Boolean")) {
					vectorSize = IoBox.FindPin("Vector Size").Spread;
					if (vectorSize == "1")
						subType = "xSlider";
					else if (vectorSize == "2")
						subType = "xySlider";
					else if (vectorSize == "3" )
						subType = "xyzSlider";
					else
						subType = "xyzSlider";
				}
				//set Tags
				tag = IoBox.FindPin("Tag").Spread;
				if (tag =="||")
					tag="";
				// set default Value
				defaultValue = IoBox.FindPin("Y Input Value").Spread;
				//set Gui Height
				guiHeight = "300"; //iobox.GetBounds(BoundsType.Box).Height.ToString();
				if (subType == "xySlider" || subType == "xyzSlider")
					guiHeight = "900";
				// set Allow Multiple
				allowMultiple = "0";
				// set Min/Max Values
				if (vvvvValueType == "Boolean"){
					minValue = "0";
					maxValue = "1";
				}
				else{
					minValue = IoBox.FindPin("Minimum").Spread;
					maxValue = IoBox.FindPin("Maximum").Spread;
				}
			}
			
			return subType;
		}
		
	private void DictionaryChanged(object sender, EventArgs e)
	{
		if (FDict[0]!=null)
		{
			FUpdate=true;
		}
	}
	}
	#endregion GetParameters
	
	
	#region ObservableDictionaryClass
	public delegate void CollectionAlteredEventHander( object sender , EventArgs e);
	public partial class ObservableDictionary<TKey, TValue> : Dictionary<TKey, TValue>
	{
		
		/*Class contructors*/
		
		public event CollectionAlteredEventHander CollectionAltered;
		
		public new TValue this[TKey key]
		{
			get
			{
				return base[key];
			}
			set
			{
				OnCollectionAltered(new EventArgs());
				base[key] = value;
			}
		}
		
		public new void Add(TKey key, TValue value)
		{
			int idx = 0;
			if (!TryGetKeyIndex(this, key, ref idx))
			{
				base.Add(key, value);
				OnCollectionAltered(new EventArgs());
			}
		}
		
		public new bool Remove(TKey key)
		{
			int idx = 0;
			if( TryGetKeyIndex( this ,key, ref idx))
			{
				OnCollectionAltered(new EventArgs());
				return base.Remove(key);
			}
			return false;
		}
		
		private bool TryGetKeyIndex(ObservableDictionary<TKey, TValue> observableDictionary, TKey key , ref int idx)
		{
			foreach (KeyValuePair<TKey, TValue> pair in observableDictionary)
			{
				if (pair.Key.Equals(key))
				{
					return true;
				}
				idx++;
			}
			return false;
		}
		
		public new void Clear()
		{
			OnCollectionAltered(new EventArgs());
			base.Clear();
		}
		
		protected virtual void OnCollectionAltered(EventArgs e)
		{
			if (CollectionAltered != null)
			{
				CollectionAltered(this, e);
			}
		}
		
	}
	#endregion ObservableDictionaryClass
}


