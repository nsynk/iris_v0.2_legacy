#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "MenuItems", Category = "Value", Help = "Basic template with one value in/out", Tags = "")]
	#endregion PluginInfo
	public class ValueMenuItemsNode : IPluginEvaluate
	{
		#region fields & pins
		
		List <Item> menuItems = new List<Item>();
		Item item;
		
		[Input("Height", DefaultValue = 10)]
		IDiffSpread<int> FHeight;
		
		[Input("Width", DefaultValue = 10)]
		IDiffSpread<int> FWidth;
		
		[Input("Space", DefaultValue = 2)]
		IDiffSpread<int> FSpace;
		
		[Input("Count", DefaultValue = 1)]
		IDiffSpread<int> FCount;
		
		[Input("IsToggle")]
		IDiffSpread<bool> FIsToggle;
		
		[Input("Initial State")]
		IDiffSpread<bool> FInitialState;
		
		[Input("Mouse")]
		IDiffSpread<Vector3D> FMouse;
		
		[Output("Pos")]
		ISpread<Vector2D> FPos;
		
		[Output("Scale")]
		ISpread<Vector2D> FScale;
		
		[Output("Mouse Over")]
		ISpread<bool> FMouseOver;
		
		[Output("State")]
		ISpread<bool> FState;
		
		[Import()]
		ILogger FLogger;
		#endregion fields & pins
		
		//called when data for any o utput pin is requested
		public void Evaluate(int SpreadMax)
		{
			FPos.SliceCount = FCount[0];
			FScale.SliceCount = FCount[0];
			FMouseOver.SliceCount = FCount[0];
			FState.SliceCount = menuItems.Count;
			
			if ((FCount.IsChanged || FHeight.IsChanged || FWidth.IsChanged || FSpace.IsChanged) && FCount[0] > 0)
			{
				menuItems.Clear();
				for (int i=0; i < FCount[0];i++)
				{
					if (i == 0)
						item = new Item(0, FWidth[i], -FHeight[i], FIsToggle[i], FInitialState[i]);
					else
						item = new Item(menuItems[i-1].X + menuItems[i-1].Width + FSpace[i], FWidth[i], -FHeight[i], FIsToggle[i], FInitialState[i]);
					
					menuItems.Add(item);
				}
			}	
			
			if (FIsToggle.IsChanged)
			{
				foreach (var item in menuItems)
				{
					item.IsToggle = FIsToggle[menuItems.IndexOf(item)];
				}
			}
			
			if (FMouse.IsChanged)
			{
				foreach (var item in menuItems)
				{
					int i = menuItems.IndexOf(item);
		
					
					if (!item.IsToggle && item.IsOn)
						item.IsOn = false;

					if ((FMouse[0].x > item.X && FMouse[0].x < item.X + item.Width) &&
						(FMouse[0].y < 0 && FMouse[0].y > item.Height))
					{
						FMouseOver[i] = true;
						if (FMouse[0].z == 1 && item.IsToggle && !item.IsOn)
						{
							item.IsOn = true;
						}
						else if (FMouse[0].z == 1 && item.IsToggle && item.IsOn)
						{
							item.IsOn = false;
						}
						else if (FMouse[0].z == 1 && !item.IsToggle)
						{
							item.IsOn = true;
						}
					}
					else
						FMouseOver[i] = false;
					
					
				}
				
				
			}
			
			foreach (var item in menuItems)
				{
					int i = menuItems.IndexOf(item);
					FPos[i] = new Vector2D(item.X, item.Height);
					FScale[i] = new Vector2D(item.Width, -item.Height);
					FState[i] = item.IsOn;
				}
			
			
			
			//FLogger.Log(LogType.Debug, "hi tty!");
		}
	}
	
	public class Item
	{
		private int _x;
		public int X
		{
			get { return _x; }
			set { _x = value; }
		}
		
		private int _width;
		public int Width
		{
			get { return _width; }
			set { _width = value; }
		}
		
		private int _height;
		public int Height
		{
			get { return _height; }
			set { _height = value; }
		}
		
		private bool _isToggle;
		public bool IsToggle
		{
			get { return _isToggle; }
			set { _isToggle = value; }
		}
		
		private bool _isOn;
		public bool IsOn
		{
			get { return _isOn; }
			set { _isOn = value; }
		}
		
		
		public Item (int x, int width, int height, bool isToggle, bool initialState)
		{
			_x = x;
			_width = width;
			_height = height;
			_isToggle = isToggle;
			_isOn = initialState;
		}
	}
}
