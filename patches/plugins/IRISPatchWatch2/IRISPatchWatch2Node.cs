#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;
using System.Drawing;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "PatchWatch2", Category = "IRIS", Help = "", Tags = "", AutoEvaluate = true)]
	#endregion PluginInfo
	public class IRISPatchWatch2Node : IDisposable, IPluginEvaluate
	{
		#region fields & pins
		[Input("NODE ID", IsSingle = true)]
		ISpread<string> FNodeID;
		
		[Input("NODE PATH", IsSingle = true)]
		ISpread<string> FNodePath;
		
		[Input("Update GUI", IsBang = true)]
		ISpread<bool> FUpdate;
		
		[Input("Do Log", IsToggle = false)]
		ISpread<bool> FDoLog;
		
		[Output("Output")]
		ISpread<string> FOutput;
		[Output("ID")]
		ISpread<string> FID;
		[Output("Name")]
		ISpread<string> FNAME;
		[Output("Left")]
		ISpread<string> FLEFT;
		[Output("Top")]
		ISpread<string> FTOP;
		[Output("Width")]
		ISpread<string> FWIDTH;
		[Output("Height")]
		ISpread<string> FHEIGHT;
		
		[Output("InputPin")]
		ISpread<string> FINPUTPIN;
		
		[Output("OutputPin")]
		ISpread<string> FOUTPUTPIN;
		
		[Output("Connections")]
		ISpread<string> FCONNECT;
		
		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		
		int nodeID;
		private INode2 FPatch;
		private bool FFirstFrame = true;
		private Dictionary<string, INode2> FNodes = new Dictionary<string, INode2>();
		private string debug;
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		#endregion fields & pins
		
		string nodePath;
		string handleOut;
		#region constructor/destructor
		public IRISPatchWatch2Node()
		{
		}
		
		~IRISPatchWatch2Node()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!FDisposed) {
				if (disposing) {
					// Dispose managed resources.
					//	FPatch.Added -= NodeAddedCB;
					//	FPatch.Removed -= NodeRemovedCB;
					
					//unregister all ioboxes
					foreach (var node in FPatch)
					if (node.ID == nodeID)
					RemoveNodes(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor
		
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			
			//update can be forced
			if (FUpdate[0] || FFirstFrame){
				if (!FFirstFrame )
					FNodes.Clear();
				
				nodePath = FNodePath[0];
				var node = FHDEHost.GetNodeFromPath(nodePath);
				
				if (node != null)
				{
					FPatch = node.Parent;
					if (FNodeID[0].Length == 0)
					nodeID = 0;
					else
					nodeID = Convert.ToInt32(FNodeID[0]);
					
					UpdateAllInputs();
					FOutput.AssignFrom(FNodes.Keys);
					
					
					int index=0;
					FNAME.SliceCount=FNodes.Count;
					FID.SliceCount=FNodes.Count;
					FWIDTH.SliceCount=FNodes.Count;
					FHEIGHT.SliceCount=FNodes.Count;
					FLEFT.SliceCount=FNodes.Count;
					FTOP.SliceCount=FNodes.Count;
					FTOP.SliceCount=FNodes.Count;
					FINPUTPIN.SliceCount=FNodes.Count;
					FOUTPUTPIN.SliceCount=FNodes.Count;
					FCONNECT.SliceCount=FNodes.Count;
					
					foreach (var myNode in FNodes) {
						
						FNAME[index] = getNodeName(myNode.Value);
						FID[index] = getNodeID(myNode.Value);
						FWIDTH[index] = getNodeWidth(myNode.Value);
						FHEIGHT[index] = getNodeHeight(myNode.Value);
						FTOP[index] = getNodeTop(myNode.Value);
						FLEFT[index] = getNodeLeft(myNode.Value);
						FINPUTPIN[index] = GetInputPins (myNode.Value);
						FOUTPUTPIN[index] = GetOutputPins (myNode.Value);
						FCONNECT[index] = GetConnectedPins (myNode.Value);
						index++;
					}
					
					FFirstFrame = false;
					
				}
			}
		}
		
		private void UpdateAllInputs()
		{
			//go through all nodes in this patch
			//if it is a node from a given path
			//extract its inputs
			foreach (var node in FPatch) {
				if (node.ID == nodeID)
				AddNodes(node);
			}
		}
		
		private void AddNodes(INode2 patch){
			///
			/// PARSE SANDBOX
			///
			if (FDoLog[0]){
				FLogger.Log(LogType.Debug, " ");
				FLogger.Log(LogType.Debug, " ");
				FLogger.Log(LogType.Debug, "############################################");
				FLogger.Log(LogType.Debug, ">> ParsePatch: " + patch.Name);
				FLogger.Log(LogType.Debug, "############################################");
			}
			foreach (var node in patch) {
				var param = node.ID.ToString();
				if ((!FNodes.ContainsValue(node))
				&& (node.NodeInfo.Filename.Contains(".v4p"))) {
					
					if (FDoLog[0])
					FLogger.Log(LogType.Debug, ">> AddNode: " + node.Name + "   ID: " + node.ID.ToString());
					
					
					param += "|" + GetNodeInfo(node) + AddPins(node) + "|";
					FNodes.Add(param, node);
				}
			}
			if (FDoLog[0])
			FLogger.Log(LogType.Debug, "--------------------------------------------");
		}
		
		private string AddPins(INode2 patch){
			
			string inputPins ="";
			string outputPins ="";
			
			if (FDoLog[0]){
				FLogger.Log(LogType.Debug, "--------------------------------------------");
				FLogger.Log(LogType.Debug, "PINS FOR: " + patch.Name);
				FLogger.Log(LogType.Debug, " ");
			}
			foreach (var node in patch){
				
				if ((node.NodeInfo.Name == "IOBox")
				&& (node.NodeInfo.Category == "Node")
				&& (patch.Name.Contains("EX9"))){
					
					var input = node.FindPin("Input Node");
					var output = node.FindPin("Output Node");
					if (FDoLog[0]){
						FLogger.Log(LogType.Debug, "INPUT: " + input.ParentNode.Name + " ID: " + input.ParentNode.ID + "  Conected: " + input.IsConnected().ToString());
						FLogger.Log(LogType.Debug, "OUTPUT: " + output.ParentNode.Name + " ID: " + output.ParentNode.ID + "  Conected: " + output.IsConnected().ToString());
					}
					// only input pin is connected in own patch
					if (input.IsConnected() && !output.IsConnected() && node.LabelPin.Spread.Trim('|') != "" ){
						foreach (var pin in input.ConnectedPins){
							
							if (FDoLog[0])
							FLogger.Log(LogType.Debug, ">> Conected Nodes to Input Pin : " + pin.ParentNode.Parent.Parent.ID + " " + node.Parent.ID);
							
							if (pin.ParentNode.Parent.ID == node.Parent.ID || pin.ParentNode.Parent.Parent.ID == node.Parent.ID || pin.ParentNode.ID == node.Parent.ID)
							outputPins += GetPinLable(node) + ";";
							else
							inputPins += GetPinLable(node) + "#" + pin.ParentNode.Parent.ID+ ", " + GetPinLable(pin.ParentNode) +  ";";
						}
					}
					else
					inputPins += GetPinLable(node) + ";";
					
					
					
				}
			}
			return  inputPins + "|" + outputPins;
		}
		
		private string GetInputPins(INode2 patch)
		{
			string tmp ="";
			
			foreach (var node in patch){
				var input = node.FindPin("Input Node");
				var output = node.FindPin("Output Node");
				
				if (node.NodeInfo.Name == "IOBox" && node.NodeInfo.Category == "Node" && patch.Name.Contains("EX9") && node.LabelPin.Spread.Trim('|') != "" )
				{
					string pins="";
					
					foreach (var cPin in output.ConnectedPins)
					{
						if ((cPin.ParentNode.Parent.GetNodePath(true).Contains(patch.GetNodePath(true))))
						{
							tmp += patch.ID.ToString() + "-" + GetPinLable(node) + "; ";
						}
						break;
					}
				}
			}
			return tmp;
		}
		
		private string GetOutputPins(INode2 patch)
		{
			string tmp ="";
			foreach (var node in patch){
				var input = node.FindPin("Input Node");
				var output = node.FindPin("Output Node");
				
				if (node.NodeInfo.Name == "IOBox" && node.NodeInfo.Category == "Node" && patch.Name.Contains("EX9") && node.LabelPin.Spread.Trim('|') != "" && input.IsConnected())
				{
					string pins="";
					
					foreach (var cPin in input.ConnectedPins)
					{
						if ((cPin.ParentNode.Parent.GetNodePath(true).Contains(patch.GetNodePath(true))) )
						{
							tmp += patch.ID.ToString() + "-" + GetPinLable(node) + "; ";
						}
					}
				}
			}
			return tmp;
		}
		
		private string GetConnectedPins(INode2 patch)
		{
			string tmp ="";
			foreach (var node in patch){
				var input = node.FindPin("Input Node");
				var output = node.FindPin("Output Node");
				
				if (node.NodeInfo.Name == "IOBox" && node.NodeInfo.Category == "Node" && patch.Name.Contains("EX9") && node.LabelPin.Spread.Trim('|') != "" && input.IsConnected())
				{
					string pins="";
					
					foreach (var cPin in output.ConnectedPins)
					{
						if (!(cPin.ParentNode.Parent.GetNodePath(true).Contains(patch.GetNodePath(true))) && cPin.ParentNode.Parent.Name.Contains("EX9") )
						{
							tmp += patch.ID.ToString() + "-" + GetPinLable(node) + ":" + cPin.ParentNode.Parent.ID + "-" + GetPinLable(cPin.ParentNode) + ";";
						}
					}
				}
			}
			return tmp;
		}
		
		private string GetPinLable(INode2 iobox)
		{
			return iobox.LabelPin.Spread.Trim('|');
		}
		
		private string getNodeName(INode2 node)
		{
			var tmp = "";
			tmp = node.Name;
			return tmp;
		}
		
		private string getNodeID(INode2 node)
		{
			var tmp = "";
			tmp = node.ID.ToString();
			return tmp;
		}
		private string getNodeWidth(INode2 node)
		{
			var tmp = "";
			tmp = node.GetBounds(BoundsType.Node).Width.ToString();
			return tmp;
		}
		private string getNodeHeight(INode2 node)
		{
			var tmp = "";
			tmp = node.GetBounds(BoundsType.Node).Height.ToString();
			return tmp;
		}
		private string getNodeLeft(INode2 node)
		{
			var tmp = "";
			tmp = node.GetBounds(BoundsType.Node).Left.ToString();
			return tmp;
		}
		private string getNodeTop(INode2 node)
		{
			var tmp = "";
			tmp = node.GetBounds(BoundsType.Node).Top.ToString();
			return tmp;
		}
		
		private string GetNodeInfo(INode2 node)
		{
			var name = "";
			var left = "";
			var top = "";
			var width = "";
			var height = "";
			string inputPins ="";
			string outputPins ="";
			name = node.Name;
			
			left = node.GetBounds(BoundsType.Node).Left.ToString();
			top = node.GetBounds(BoundsType.Node).Top.ToString();
			width = node.GetBounds(BoundsType.Node).Width.ToString();
			height = node.GetBounds(BoundsType.Node).Height.ToString();
			
			
			
			return name + "|" + left + "|" + top + "|" + width + "|" + height + "|";
		}
		
		private void RemoveNodes(INode2 patch)
		{
			//remove all inputs of that node
			var nodePath = patch.GetNodePath(false);
			var keysToDelete = new List<string>();
			foreach (var key in FNodes.Keys)
			if (key.StartsWith(nodePath))
			keysToDelete.Add(key);
			
			foreach (var key in keysToDelete) {
				var node = FNodes[key];
				FNodes.Remove(key);
			}
		}
	}
}
