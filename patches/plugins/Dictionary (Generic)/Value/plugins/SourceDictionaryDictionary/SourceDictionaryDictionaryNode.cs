#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	public class Dictionary : IPluginEvaluate
	{
		public Dictionary<string, ISpread<double>> dict = new Dictionary <string, ISpread<double>>();
		
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
		}
	}

	[PluginInfo(Name = "Dictionary", Category = "Dictionary", Version = "Value", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	public class CreateDictionary : IPluginEvaluate
	{
		[Input("Clear", IsBang = true, IsSingle = true)]
		ISpread<bool> FClear;
		
		[Output("Dictionary")]
		ISpread<Dictionary> FOutput;
		
		[Output ("Key Count")]
		ISpread<double> FOutputKeys;
		
		bool firstFrame = true;
		//public GenericDictionary dict;
		//called when data for any output pin is requested
		
		
		public void Evaluate(int SpreadMax)
		{
	
			if (firstFrame || FClear[0]){
				FOutput.SliceCount = 0;
				var dict = new Dictionary();
				dict.dict.Clear();
				FOutput.Add(dict);
				firstFrame = false;
			}
			FOutputKeys[0] = FOutput[0].dict.Count;
		}
	}
	
	[PluginInfo(Name = "SetKey", Category = "Dictionary", Version = "Value", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	public class SetKey : Dictionary, IPluginEvaluate
	{
		[Input("Input")]
		ISpread<Dictionary> FDict;
		
		[Input("DoSet", IsBang = true)]
		ISpread<bool> FDoSet;
		
		[Input ("InputValue")]
		ISpread<ISpread<double>> FInputValue;
		
		[Input("SetID")]
		ISpread<string> FSetID;
	
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FDoSet[0])
			{
				for (int i=0; i<FSetID.SliceCount; i++)
				{
					if(FDict[0].dict.ContainsKey(FSetID[i]))
						FDict[0].dict.Remove(FSetID[i]);
					FDict[0].dict.Add(FSetID[i], FInputValue[i].Clone());
				}
				
			}	
		}
	}
	
	[PluginInfo(Name = "GetKey", Category = "Dictionary", Version = "Value", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	public class GetKey : Dictionary, IPluginEvaluate
	{
		[Input("Input")]
		ISpread<Dictionary> FDict;
		
		[Input("GetID")]
		ISpread<string> FGetID;
		
		[Input("DoGet", IsBang = true)]
		ISpread<bool> FDoGet;
		
		[Output("Output Value")]
		ISpread<ISpread<double>> FOutputValue;
		
		[Input ("Default")]
		ISpread<double> FDefaultValue;
			
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FDoGet[0])
			{
				FOutputValue.SliceCount=FGetID.SliceCount;
				FDefaultValue.SliceCount=FGetID.SliceCount;
				for (int i=0; i<FGetID.SliceCount; i++)
				{
					if (FDict[0].dict.ContainsKey(FGetID[i]))
						FOutputValue[i] = FDict[0].dict[FGetID[i]];
					else
						FOutputValue[i][i] = FDefaultValue[i];
				}
				
			}
			
		}
	}
	
	[PluginInfo(Name = "RemoveKey", Category = "Dictionary", Version = "Value", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	public class RemoveKey : Dictionary, IPluginEvaluate
	{
		[Input("Input")]
		ISpread<Dictionary> FDict;
		
		[Input("ID")]
		ISpread<string> FDeleteID;
		
		[Input("DoDelete", IsBang = true)]
		ISpread<bool> FDoDelete;
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FDoDelete[0])
			{
				for (int i = 0; i< FDeleteID.SliceCount; i++)
				{
					if(FDict[0].dict.ContainsKey(FDeleteID[i]))
					{
						FDict[0].dict.Remove(FDeleteID[i]);
					}
				}
			}
		}
	}
	
	[PluginInfo(Name = "Dump", Category = "Dictionary", Version = "Value", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	public class Dump : Dictionary, IPluginEvaluate
	{
		[Input("Input")]
		ISpread<Dictionary> FDict;
		
		[Output ("OutputDumpID")]
		ISpread<string> FOutputDumpID;
		
		[Output ("OutputDumpValue")]
		ISpread<ISpread<double>> FOutputDump;
		
		[Input("DoDump", IsBang = true, IsSingle = true)]
		ISpread<bool> FDump;
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			if (FDump[0])
			{
				FOutputDumpID.SliceCount=FDict[0].dict.Count;
				FOutputDump.SliceCount=FDict[0].dict.Count;
				
				int i=0;
				foreach (ISpread<double> v in FDict[0].dict.Values)
				{
					FOutputDump[i] = v;
					i++;
				}
				int j=0;
			
				foreach (string v in FDict[0].dict.Keys)
				{
					FOutputDumpID[j]=v.ToString();
					j++;
				}	
			}
		}
	}
	
}

