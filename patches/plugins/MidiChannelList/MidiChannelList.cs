
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using System.Xml.Linq;
using VVVV.Core.Logging;
using AmazedSaint.Elastic.Lib;
using Microsoft.CSharp.RuntimeBinder;


namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "MidiChannelList", Category = "String", Help = "handles the lists", Tags = "")]
	#endregion PluginInfo


	public class MidiChannelList : IPluginEvaluate
	{
		
	
		[Input("PreloadList")]
		IDiffSpread<string> FPreloadList;
		
		
		[Input("InputAddress")]
		ISpread<string> FInputAddress;	
		
		[Input("InputChannel")]
		ISpread<string> FInputChannel;
		
		[Input("InputMidiValues")]
		ISpread<string> FInputMidiValues;
		
		[Input("Input TypeTag")]
		ISpread<string> FInputTypeTag;
		
		
		[Input("CurrentlyLoadedPatches")]
		IDiffSpread<string> FCurrentlyLoadedPatches;

		[Input("DoubleClick", IsBang=true)]
		IDiffSpread<bool> FDoubleClick;

		[Input("MouseLeftPressed")]
		ISpread<bool> FMouseLeftPressed;

		[Input("MIDILearnOn")]
		ISpread<bool> FMidiLearnOn;
		
		//-----------------
		
		[Output("All Addresses")]
		ISpread<string> FOutputAddress;
		
		[Output("All Channels")]
		ISpread<string> FOutputChannel;
		
		[Output("All TypeTags")]
		ISpread<string> FOutputTypeTags;
		
		[Output("Currently Touched Channel")]
		ISpread<string> FCurrentlyTouchedChannel;
	
		[Output("Current Values")]
		ISpread<string> FCurrentValues;	

		[Output("Current TypeTag")]
		ISpread<string> FCurrentTypeTag;		
	
		[Output("Current Indexes")]
		ISpread<int> FCurrentlyTouchedIndexes;
		
		[Output("Waiting Addresses")]
		ISpread<string> FWaitingAddresses;
		
		[Output("XML Output")]
		ISpread<string> FCurrentXMLOutput;
		
		[Output("ListIsSet", IsBang=true)]
		ISpread<bool> FListIsSet;
		
		[Import()]
		ILogger Flogger;

		List<string> FIncomingAddresses = new List<string>();
		
		List<string> FAllAddresses = new List<string>();
		List<string> FAllChannels = new List<string>();
		List<string> FALLTypeTags = new List<string>();	
		List<int> FCurrentIndexes = new List<int>();
		List<string> CurrentlyTouchedChannel = new List<string>();
		List<string> CurrentValues = new List<string>();
		List<string> CurrentTypeTag = new List<string>();
		
		bool alreadyinlist = false;
		bool selectionset = false;
		bool alreadyinaddresslist = true;
		
		
		public void inizialize()
		{
			if (FPreloadList.SliceCount>0)
			{
				int nchannels=FPreloadList.SliceCount/3;
				FAllAddresses.Clear();
				FAllChannels.Clear();
				FALLTypeTags.Clear();
			
				for (int i=0; i<nchannels; i++)
				{	
					FAllAddresses.Add(FPreloadList[i]);	
					FAllChannels.Add(FPreloadList[i + nchannels]);
					FALLTypeTags.Add(FPreloadList[i + 2 * nchannels]);
				}
				GetCurrentIndexes();
			}
		}
		
		public void CollectAddressesAndWaitForChannelandXYZ()
			{
				alreadyinaddresslist=false;
				{	
			    	if (FInputAddress.SliceCount > 0 )
					{	
							if (FIncomingAddresses.Count>0)
							for (int i=0; i<FIncomingAddresses.Count; i++)
								if (FInputAddress[0] == FIncomingAddresses[i])
								{
									FIncomingAddresses.RemoveAt(i);//(FInputAddress[0]);	
									FIncomingAddresses.Add(FInputAddress[0]);				
									alreadyinaddresslist=true;					
								}
								if (FIncomingAddresses.Count==0)
								{
									FIncomingAddresses.Add(FInputAddress[0]);
									alreadyinaddresslist=true;
								}
								if (alreadyinaddresslist==false)
								{
									FIncomingAddresses.Add(FInputAddress[0]);				
									alreadyinaddresslist=true;
								}
					}					
						//		FDebug3.SliceCount=FIncomingXYZ.Count;
           			//	for (int i=0; i<FIncomingXYZ.Count; i++)
           			//		FDebug3[i] = FIncomingXYZ[i];
						selectionset = true;			
						FWaitingAddresses.AssignFrom(FIncomingAddresses);
					}									
				
			}
		public void SetChannels()
		{		
			alreadyinlist=false;
			if (FInputChannel.SliceCount > 0 && selectionset && FMidiLearnOn[0] && FIncomingAddresses.Count>0)
					{
						for (int i=0; i<FIncomingAddresses.Count; i++)
						{
							if (FAllAddresses.Count>0)
							{						
							for (int j=0; j<FAllAddresses.Count; j++)	
								if(FAllAddresses[j]==FIncomingAddresses[i] && FAllChannels[j]==FInputChannel[0])
									{
									FIncomingAddresses.RemoveAt(i);						
									i--;
									}
							}							
        	   				FAllAddresses.Add(FIncomingAddresses[i]);       	   	
      	     				FAllChannels.Add(FInputChannel[0]);     
							FALLTypeTags.Add(FInputTypeTag[0]);     
           				}
						FIncomingAddresses.Clear();
						FWaitingAddresses.AssignFrom(FIncomingAddresses);
						selectionset = false;
						FListIsSet[0]=true;
						GetCurrentIndexes();
				}			
				
		}
		public void FreeChannel()
		{
			if (selectionset && FIncomingAddresses.Count==1)
				{	
				for (int i=0; i<FAllAddresses.Count; i++)
			 	 {
			 	 		if (FAllAddresses[i]==FIncomingAddresses[0])
			 	 			{
			 	 				FAllAddresses.RemoveAt(i);
								FAllChannels.RemoveAt(i);		
								FALLTypeTags.RemoveAt(i);
								i--;
			 	 			}
			 	 }
			 	 	FListIsSet[0]=true;	
			 	 	FIncomingAddresses.Clear();
		
			 	 	GetCurrentIndexes();
			 	 }
		}
		public void GetCurrentIndexes()	
		{

			FCurrentIndexes.Clear();
			Dictionary<string, int> d = new Dictionary<string, int>();

				
			for (int i=0; i<FCurrentlyLoadedPatches.SliceCount; i++) 
			{
				 d.Add(FCurrentlyLoadedPatches[i],i);
			}
						
			for (int i=0; i<FAllAddresses.Count; i++)  
			{				
				if (d.ContainsKey(FAllAddresses[i]))         
				FCurrentIndexes.Add(d[FCurrentlyLoadedPatches[i]]);		
			}
			FCurrentlyTouchedIndexes.AssignFrom(FCurrentIndexes);		
		}

		public void outputmessage()
		{
			CurrentlyTouchedChannel.Clear();
			CurrentValues.Clear();
			CurrentTypeTag.Clear();
			for (int i=0; i<FInputChannel.SliceCount; i++)
			{
				for (int j=0; j<FCurrentIndexes.Count; j++)
				{	
					if (FInputChannel[i]==FAllChannels[FCurrentIndexes[j]])
					{			
								CurrentlyTouchedChannel.Add(FAllAddresses[FCurrentIndexes[j]]);//+"/"+0+"-"+String.Format(System.Globalization.CultureInfo.CreateSpecificCulture("en-US"),"{0:0.0000}", (FInputMidiValues[FAllChannels[FCurrentIndexes[j]]])));
								CurrentValues.Add(FInputMidiValues[i]); 
								CurrentTypeTag.Add(FInputTypeTag[FCurrentIndexes[j]]);
					}	
			
				}
				;
			}
			FCurrentlyTouchedChannel.AssignFrom(CurrentlyTouchedChannel);
			FCurrentValues.AssignFrom(CurrentValues);
			FCurrentTypeTag.AssignFrom(CurrentTypeTag);
		}	
		
		public void CreateXML()
		{
			dynamic midiXML = new ElasticObject("IrisMidiXML");	
			for (int i=0; i<FOutputAddress.SliceCount; i++)
				{
					var kanal = midiXML <<"Set";
					kanal.OSCChannel=FAllAddresses[i];
					kanal.ChannelDescription = FAllChannels[i];
					kanal.TypeTags = FALLTypeTags[i];
		
				}
				
			XElement el = midiXML > FormatType.Xml;
			FCurrentXMLOutput.SliceCount = 1;
			FCurrentXMLOutput[0] = Convert.ToString(el);
				
			}
		
		public void Evaluate(int SpreadMax)
		{
			FListIsSet.SliceCount=1;
			FListIsSet[0]=false;	
			if (FPreloadList.IsChanged)
				{
					inizialize();
				}
	
			if (FMidiLearnOn[0] && FMouseLeftPressed[0])
				CollectAddressesAndWaitForChannelandXYZ();
			if (FMidiLearnOn[0] && FDoubleClick[0])
				FreeChannel();
			if (FMidiLearnOn[0] &&!FMouseLeftPressed[0])
				SetChannels();
			if (FCurrentlyLoadedPatches.IsChanged)
				GetCurrentIndexes();
			FCurrentlyTouchedChannel.SliceCount=0;
			FCurrentValues.SliceCount=0;
			FCurrentTypeTag.SliceCount=0;
			if (FInputChannel.SliceCount>0 && FMidiLearnOn[0]==false )
			{
				outputmessage();
				FIncomingAddresses.Clear();
			
			}
			if ( FMidiLearnOn[0]==false )
			{		
				FIncomingAddresses.Clear();		
				
			}
			FWaitingAddresses.AssignFrom(FIncomingAddresses);
	
			FOutputAddress.AssignFrom(FAllAddresses);
			FOutputChannel.AssignFrom(FAllChannels);
			FOutputTypeTags.AssignFrom(FALLTypeTags);
			if (FListIsSet[0])
				CreateXML();	
		}
	}
}
