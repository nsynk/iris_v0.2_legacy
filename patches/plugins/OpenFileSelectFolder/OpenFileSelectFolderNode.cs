using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;

namespace VVVV.Nodes
{
	[PluginInfo(Name = "SelectFolder", Category = "File", Version = "Open", Help = "Opens a file dialog (without blocking vvvv)", Tags = "", Author = "vux, vvvv group", AutoEvaluate = true)]
	public class OpenFileSelectFolderNode : IPluginEvaluate, IDisposable
	{
		#region fields & pins
		private IPluginHost FHost;

		[Input("Open", IsSingle = true, IsBang = true)]
		ISpread<bool> FPinInOpen;

		[Output("Path", StringType = StringType.Filename)]
		ISpread<string> FPinOutPath;

		[Output("Openend", IsBang = true, IsSingle = true)]
		ISpread<bool> FBangOut;

		private Thread FThread;

		private FolderBrowserDialog FDialog = new FolderBrowserDialog();

		private event EventHandler OnSelect;

		private bool FOpened = false;
		private bool FInvalidate = false;
		#endregion

		#region Evaluate
		public void Evaluate(int SpreadMax)
		{
			FBangOut[0] = false;

			if (FPinInOpen[0] && !FOpened) {
				
				FDialog.Description = "Select a new IRIS Project folder";
				FDialog.RootFolder = Environment.SpecialFolder.DesktopDirectory;
			//	FDialog.CheckPathExists = FCheckPathExists[0];
			//	FDialog.CheckFileExists = FCheckPathExists[0];
				
				OnSelect -= new EventHandler(FileDialogNode_OnSelect);
				OnSelect += new EventHandler(FileDialogNode_OnSelect);
				FOpened = true;
				FThread = new Thread(new ThreadStart(DoOpen));
				FThread.SetApartmentState(ApartmentState.STA);
				FThread.Start();
			}
			if (FPinInOpen[0] && FOpened) {
				//FDialog.ShowDialog();
			}

			if (FInvalidate) {
				
				FPinOutPath[0] = FDialog.SelectedPath;
				FBangOut[0] = true;
				FInvalidate = false;
				

       		
			}
		}

		void FileDialogNode_OnSelect(object sender, EventArgs e)
		{
			FInvalidate = true;
		}
		#endregion

		#region Dispose
		public void Dispose()
		{
			if (FOpened) {
				try {
					FThread.Abort();
				} catch {

				}
			}
		}
		#endregion

		#region Do Open
		private void DoOpen()
		{
			if (FDialog.ShowDialog() == DialogResult.OK) {
				if (OnSelect != null) {
					OnSelect(this, new EventArgs());
				}
			}
			FOpened = false;
		}
		#endregion
	}
}
