#region usings
using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel.Composition;
using System.Drawing;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.PluginInterfaces.V2.Graph;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;
using VVVV.Utils.OSC;

using VVVV.Core;
using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "GetNodePos",
	Category = "IRIS",
	Help = "", Tags = "",
	AutoEvaluate = true)]
	#endregion PluginInfo
	public class ValueGetNodePosNode :  IDisposable, IPluginEvaluate
	{
			#region fields & pins
		[Input("NODE ID", IsSingle = true)]
		ISpread<string> FNodeID;
		
		[Input("NODE PATH", IsSingle = true)]
		ISpread<string> FNodePath;
		
		[Input("Update GUI", IsBang = true)]
		ISpread<bool> FUpdate;
		
		[Input("Do Log", IsToggle = false)]
		ISpread<bool> FDoLog;
		
		[Output("Output")]
		ISpread<string> FOutput;
		
		[Output("Handles")]
		ISpread<string> FHandle;
		
		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHDEHost;
		
		[Import()]
		IPluginHost FPluginHost;
		
		int nodeID;
		private INode2 FPatch;
		private bool FFirstFrame = true;
		private Dictionary<string, INode2> FNodes = new Dictionary<string, INode2>();
		private string debug;
		// Track whether Dispose has been called.
		private bool FDisposed = false;
		#endregion fields & pins

		#region constructor/destructor
		public ValueGetNodePosNode()
		{
		}
		
		~ValueGetNodePosNode()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
		}
		
		protected void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!FDisposed) {
				if (disposing) {
					// Dispose managed resources.
				//	FPatch.Added -= NodeAddedCB;
				//	FPatch.Removed -= NodeRemovedCB;
					
					//unregister all ioboxes
					foreach (var node in FPatch)
					if (node.ID == nodeID)
					RemoveNodes(node);
				}
				// Release unmanaged resources. If disposing is false,
				// only the following code is executed.
			}
			FDisposed = true;
		}
		#endregion constructor/destructor
		
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			FOutput.SliceCount = SpreadMax;

		//	for (int i = 0; i < SpreadMax; i++)
		//		FOutput[i] = FInput[i] * 2;

			
			FLogger.Log(LogType.Debug, "hi tty!");
		}
		
		private void RemoveNodes(INode2 patch)
		{
			//remove all inputs of that node
			var nodePath = patch.GetNodePath(false);
			var keysToDelete = new List<string>();
			foreach (var key in FNodes.Keys)
			if (key.StartsWith(nodePath))
			keysToDelete.Add(key);
			
			foreach (var key in keysToDelete) {
				var node = FNodes[key];
				FNodes.Remove(key);
			}
		}
	}
}
