#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "MatchesAny", Category = "String", Help = "Similar to sift but with the right unsorted indizes", Tags = "")]
	#endregion PluginInfo
	public class StringMatchesAnyNode : IPluginEvaluate
	{
		#region fields & pins
		
		List<string> list = new List<string>();
		
		[Input("Input", DefaultString = "")]
		IDiffSpread<string> FInput;
		
		[Input("Filter", DefaultString = "")]
		IDiffSpread<string> FFilter;
		
		[Input("Update", IsBang = true, IsSingle = true)]
		IDiffSpread<bool> FUpdate;
		
		[Output("Output")]
		ISpread<int> FOutput;
		
		[Import()]
		ILogger FLogger;
		#endregion fields & pins
		
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			FOutput.SliceCount = FFilter.SliceCount;
			
			if (FInput.IsChanged || FUpdate[0])
			{
				list.Clear();
				for (int i = 0; i < FInput.SliceCount; i++)
				{
					list.Add(FInput[i]);
				}
			}
			
			if ((FFilter.IsChanged || FInput.IsChanged || FInput.IsChanged) && FFilter.SliceCount > 0 && FInput.SliceCount > 0)
			{
				for (int i=0; i < FFilter.SliceCount; i++)
				{
					foreach (string inputElement in list)
					{
						if(inputElement == FFilter[i]) {
							FOutput[i] = list.IndexOf(inputElement);
							break;
						}	
						else
						{
							FOutput[i] = -1;
						}
					}
				}
			}
		}
	}
}
