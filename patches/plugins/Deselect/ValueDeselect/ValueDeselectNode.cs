#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "Deselect", Category = "Value Spectral", Help = "Groups following equal Slices into Bins. Performs an inverse 'select' operation", Tags = "= (Equal) Value Spectral, Select, Bin")]
	#endregion PluginInfo
	public class ValueDeselectNode : IPluginEvaluate
	{
		#region fields & pins
	[Input("Input", DefaultValue = 0.0)]
		ISpread<ISpread<double>> FInput;

		[Output("Items")]
		ISpread<double> FOutputItems;

		[Output("Bin Sizes")]
		ISpread<double> FOutputBinSizes;
		
		[Import()]
		ILogger FLogger;
		#endregion fields & pins

		//called when data for any output pin is requested

		public void Evaluate(int SpreadMax)
		{
		
		List<double> Item = new List<double>();	
		List<double> SequenceCounter = new List<double>();
	
		var count = 0;
		
		for (int i = 0; i < FInput.SliceCount; i++)
		{
					
			for (int j = 0; j < FInput[i].SliceCount; j++)
			{
								
				if (FInput[i][j] == FInput[i][j+1])
				{
				count = count+1;
					if (count == FInput[i].SliceCount)
					{
					SequenceCounter.Add(count);
					Item.Add(FInput[i][j]);
					count = 0;
					}
				}
				
				else
				{
				count = count+1;
				SequenceCounter.Add(count);
				Item.Add(FInput[i][j]);
				count = 0;
				}
				
				
			}
		}
		
		FOutputBinSizes.SliceCount = SequenceCounter.Count;
		FOutputItems.SliceCount = SequenceCounter.Count;
		
		for (int i = 0; i < SequenceCounter.Count; i++)
		{
		FOutputBinSizes[i] = SequenceCounter[i];
		FOutputItems[i] = Item[i];
		}
			//FLogger.Log(LogType.Debug, "Logging to Renderer (TTY)");
		}
	}
}
