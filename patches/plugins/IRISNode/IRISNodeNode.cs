#region usings
using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
using SlimDX;
using SlimDX.Direct3D9;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "Node", Category = "IRIS", Help = "Basic template with one value in/out", Tags = "", AutoEvaluate = true)]
	#endregion PluginInfo
	public class IRISNodeNode : IPluginEvaluate
	{
		#region fields & pins
		
		Dictionary<string, Node> nodeList =  new Dictionary<string, Node>();
		List <int> dragIDs = new List<int>();
		
		Node myNode;
		
		bool FF_DRAG = false;
		bool startDrag = false;
		bool isResize = false;
		string reSizeId = "nil";
		int offsetX = 0;
		int offsetY = 0;
		bool overSizer = false;
		bool doMultiSelection;
		bool isMultiSelection;
		int MS_tmpX = 0;
		int MS_tmpY =0;
		int MS_startX = 0;
		int MS_startY =0;
		int MS_endX =0;
		int MS_endY =0;
		bool startMultiselect = true;
		bool endMultiselect = true;
		double scaleFactor;
		bool FF_LOCK = true;
		Vector2D currentInput;
		Vector3D decomposedTranslate;
		Vector3D decomposedScales;
		int lastFrameMouseClick;
		int currentMouseClick;
		bool upEdge;
		bool downEdge;
		Vector4D msCanvas = new Vector4D();
		
		[Input("CanvasTransform")]
		IDiffSpread<Matrix4x4> FCanvasTransform;
		
		[Input("ID")]
		IDiffSpread<string> FID;
		
		[Input("Node Position")]
		IDiffSpread<Vector4D> FInput;
		
		[Input("InputPin Count")]
		IDiffSpread<int> FInputPins;
		
		[Input("OutputPin Count")]
		IDiffSpread<int> FOutputPins;
		
		[Input("Node MinSize")]
		IDiffSpread<Vector2D> FMinSize;
		
		[Input("Headline Height (px)")]
		ISpread<int> FHeadlineHeight;
		
		[Input("Sizer Hitbox (px)")]
		ISpread<int> FSizer;
		
		[Input("LockSize (px)")]
		ISpread<int> FLockSize;
		
		[Input("Mouse (Canvas Transform)")]
		IDiffSpread<Vector3D> FMouseCanvas;
		
		[Input("Mouse")]
		IDiffSpread<Vector3D> FMouse;
		
		
		[Input("DoubleClick")]
		ISpread<int> FMouseDoubleClick;
		
		[Input("Initialize", IsBang = true)]
		ISpread<bool> FInitailize;
		
		[Input("Lock Canvas", IsSingle = true)]
		ISpread<bool> FLock;
		
		[Input("´Snap To Grid", IsSingle = true)]
		ISpread<bool> FSnap;
		
		[Input("ShiftKey", IsSingle = true)]
		ISpread<bool> FShift;
		
		
		[Output("Node Position")]
		ISpread<Vector4D> FOutput;
		
		[Output("isLocked")]
		ISpread<bool> FIsLocked;
		
		[Output("Canvas Scale")]
		ISpread<double> FCanvasScale;
		
		[Output("Tranfromed Node Position")]
		ISpread<Vector4D> FOutputTransformed;
		
		[Output("MultiSelection")]
		ISpread<Vector4D> FMultiSelection;
		
		[Output("DoMultiselect")]
		ISpread<bool> FDoMultiselect;
		
		[Output("isSelected")]
		ISpread<bool> FSelection;
		
		[Output("MouseOverSizer")]
		ISpread<bool> FMouseOverSizer;
		
		[Output("MouseOverHeadline")]
		ISpread<bool> FMouseOverHeadline;
		
		[Output("MouseOverNode")]
		ISpread<bool> FMouseOverNode;
		
		[Output("DoSave", IsBang = true)]
		ISpread<bool> FDoSave;
		
		[Output("Do Open Library", IsBang = true)]
		ISpread<bool> FDoOpenLibrary;
		
		[Output("", Visibility = PinVisibility.False)]
		ISpread<Vector2D> LastFrameValue;
		
		[Output("isDragSave")]
		ISpread<bool> FIsDrag;
		
		[Import()]
		ILogger FLogger;
		
		[Import()]
		IHDEHost FHost;
		#endregion fields & pins
		
		
	
		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			
			
			if (FInitailize[0])
				nodeList.Clear();
			
			FDoSave[0] = false;
			FDoOpenLibrary[0]= false;
			
			scaleFactor = FCanvasTransform[0].m11;
			FCanvasScale[0] = scaleFactor;
			// get current click for togedge calculation
			currentMouseClick = (int)FMouseCanvas[0].z;
			
			//TogEdge Calculation
			if (currentMouseClick < lastFrameMouseClick)
			{
				upEdge = false;
				downEdge = true;
			}
			else if (currentMouseClick > lastFrameMouseClick)
			{
				upEdge = true;
				downEdge = false;
			}
			else
			{
				upEdge = false;
				downEdge = false;
			}

			
			#region initializing
			
			if (FInitailize[0] || FID.IsChanged || FInput.IsChanged)
			{
				// Add Node to dictionary if not already inside
				for (int i = 0; i < FInput.SliceCount; i++)
				{
					if (!nodeList.ContainsKey(FID[i]))
					{
						myNode = new Node((int)FInput[i].x,(int)FInput[i].y,(int)FInput[i].z,(int)FInput[i].w, (int)FInputPins[i], (int)FOutputPins[i]);
						nodeList.Add( FID[i],myNode);
					}
					else if (nodeList.ContainsKey(FID[i]) && FInitailize[0]) // if already in dict just update entry
					{
						
						nodeList[FID[i]].Left = (int)FInput[i].x;
						nodeList[FID[i]].Top = (int)FInput[i].y;
						nodeList[FID[i]].Width = (int)FInput[i].z;
						nodeList[FID[i]].Height = (int)FInput[i].w;
					}
				}
			}
			#endregion initializing
			#region loop through node list
			if (FMouseCanvas.IsChanged)
			{
				overSizer = false;
				currentInput.x = FMouseCanvas[0].x;
				currentInput.y = FMouseCanvas[0].y;
				startMultiselect =true;
				//
				// LOOP THROUGH NODELIST
				//
				int selectNodeCount =0;
				if (nodeList.Count > 0 )
				{
				foreach (var node in nodeList)
				{
						
					if (!FF_DRAG && !isResize && !FLock[0]) //click
					{
						//////////////
						// HIT NODE TEST
						if (((FMouseCanvas[0].x > node.Value.Left && FMouseCanvas[0].x < node.Value.Left + node.Value.Width) &&
						(FMouseCanvas[0].y < (node.Value.Top- FHeadlineHeight[0] / scaleFactor) && FMouseCanvas[0].y > (node.Value.Top- FHeadlineHeight[0]) - node.Value.Height))&& !doMultiSelection)
						{
							node.Value.MouseOver = true;
							node.Value.MouseOverHeadline = false;
							if (FMouseCanvas[0].z == 1 && !node.Value.IsLocked)
							node.Value.Select=true;
							startMultiselect = false;	
							//isMultiSelection = false;
						}
						//////////////
						// HIT HEADLINE TEST
						else if ((FMouseCanvas[0].x > node.Value.Left && FMouseCanvas[0].x < node.Value.Left + node.Value.Width /*- FLockSize[0] / scaleFactor*/) &&
						(FMouseCanvas[0].y < node.Value.Top && FMouseCanvas[0].y > node.Value.Top - (FHeadlineHeight[0] / scaleFactor)) && !isResize && !doMultiSelection)
						{
							node.Value.MouseOverHeadline = true;
							node.Value.MouseOver = true;
							
							if(upEdge)
							{
								if (!node.Value.IsLocked)
								{
									node.Value.Select=true;	
									startMultiselect = false;
									isResize = false;
									FF_DRAG = true;
									
								}
							}
							if (FMouseDoubleClick[0] == 1 && !node.Value.IsLocked)
								FDoOpenLibrary[0]=true;
							//////////////
							// HIT LOCK TEST
							if ((FMouseCanvas[0].x > node.Value.Left + node.Value.Width - FLockSize[0] / scaleFactor && FMouseCanvas[0].x < node.Value.Left + node.Value.Width) && upEdge && FF_LOCK)
							{
								FF_LOCK = false;
								if(!node.Value.IsLocked)
								node.Value.IsLocked = true;
								else
								node.Value.IsLocked = false;
							}
						}
						else
						{
							node.Value.MouseOverHeadline = false;
							node.Value.MouseOver = false;
							
						}

						//////////////
						// HIT SIZER TEST
						if ((FMouseCanvas[0].x > node.Value.Left + node.Value.Width && FMouseCanvas[0].x < (node.Value.Left + node.Value.Width) + FSizer[0]/scaleFactor) &&
						(FMouseCanvas[0].y < node.Value.Top - node.Value.Height + FSizer[0]/scaleFactor && FMouseCanvas[0].y > node.Value.Top - node.Value.Height - FSizer[0]/scaleFactor) && !FF_DRAG && !node.Value.IsLocked)
						{
							overSizer = true;
							if (upEdge)
							{
								FF_DRAG = false;
								isResize = true;
								reSizeId = node.Key;
								node.Value.Select=true;
								startMultiselect = false;	
							}
						}
						if (FMouseCanvas[0].z==0)
						{
							FF_LOCK = true;
							FF_DRAG = false;
							
						}
					}
					
					if (!node.Value.MouseOver && upEdge )
					{
						if (!FShift[0] && !isMultiSelection)
						{
							node.Value.Select = false;
						}	
					}
					if (node.Value.Select)
						selectNodeCount++;
			
				}
				}
				#endregion loop through node list
				
				if (selectNodeCount==0)
					isMultiSelection = false;
			#region multiselect
					if (upEdge  && startMultiselect )
					{
							MS_tmpX = (int)FMouse[0].x;
							MS_tmpY = (int)FMouse[0].y;
							MS_startX = MS_tmpX;
							MS_startY = MS_tmpY;
							doMultiSelection = true;
					}
						
					// END MULTISELECT

					if (doMultiSelection)
					{
						//endMultiselect = false;
						MS_endX = (int)FMouse[0].x;
						MS_endY = (int)FMouse[0].y;
						
						if(downEdge)
						{
							MS_tmpX = 0;
							MS_tmpY = 0;
							startMultiselect = true;
							doMultiSelection = false;
						}
					}
				#endregion multiselect
				
				if ((FF_DRAG || isResize) && downEdge)
				{
					FF_DRAG = false;
					isResize = false;
					offsetX = 0;
					offsetY = 0;
					FDoSave[0]=true;
				}
				
				// DRAG SELECTED NODES NODES
				if (FF_DRAG)
				{
					foreach ( var node in nodeList)
					{
						if (node.Value.Select){
							node.Value.Left += (currentInput.x-LastFrameValue[0].x);
							node.Value.Top += (currentInput.y-LastFrameValue[0].y);
							
							// SNAP TO GRID
							if (FSnap[0])
							{
								node.Value.Left = (int)(FMouseCanvas[0].x/50)*50;
								node.Value.Top = (int)(FMouseCanvas[0].y/50)*50;
							}
						}
					}
				}
					// shift cklick multi-select
					if (FMouseCanvas[0].z == 1 && !FShift[0] && !FF_DRAG  )
					{
						foreach( var node in nodeList){
						if(!node.Value.MouseOver)
							node.Value.Select = false;
							
						}
						
					}
				
				// RESIZE NODES
				if (isResize)
				{
					overSizer = true;
					
					nodeList[reSizeId].Select=true;
					//check if nodes width is greater than minSize
					if (nodeList[reSizeId].Width >= FMinSize[0].x)
					nodeList[reSizeId].Width +=  (currentInput.x-LastFrameValue[0].x);
					else
					{
						//isResize = false;
						nodeList[reSizeId].Width = FMinSize[0].x;
						
					}
					//check if nodes heigth is greater than minSize
					if (nodeList[reSizeId].Height >= FMinSize[0].y)
					nodeList[reSizeId].Height -=  (currentInput.y-LastFrameValue[0].y);
					else
					{
						nodeList[reSizeId].Height = FMinSize[0].y;
						//isResize = false;
					}
					
				}
				
			
			}
			msCanvas = new Vector4D (MS_startX, MS_startY, MS_endX, MS_endY);
			// write NodePos to Output
			FOutput.SliceCount = nodeList.Count;
			LastFrameValue.SliceCount = nodeList.Count;
			FIsLocked.SliceCount = nodeList.Count;
			FOutputTransformed.SliceCount = nodeList.Count;
			FMouseOverNode.SliceCount = nodeList.Count;
			FMouseOverHeadline.SliceCount = nodeList.Count;
			FSelection.SliceCount = nodeList.Count;
			FMouseOverSizer[0] = overSizer;
			FDoMultiselect[0] = doMultiSelection;
			// store last frame Mouse Value for Drag
			LastFrameValue[0] = currentInput;
			
			FMultiSelection[0] =msCanvas;
			//FLogger.Log(LogType.Debug, FHost.MouseUp.ToString());
			
			if (true)
			{
				int index = 0;
				
				foreach (var node in nodeList)
				{
			
					
					FOutput[index] = new Vector4D(node.Value.Left, node.Value.Top, node.Value.Width, node.Value.Height);
					
					
					var scale = new Vector3D(0,0,0).ToSlimDXVector();
					var translate = new Vector3D(0,0,0).ToSlimDXVector();
					var temp = new Quaternion(0,0,0,0);
					
					(VMath.Transform (node.Value.Left, node.Value.Top,0,node.Value.Width, node.Value.Height,0,0,0,0) * FCanvasTransform[0]).ToSlimDXMatrix().Decompose(out scale, out temp, out translate);
					
					FOutputTransformed[index] = new Vector4D(translate.X,translate.Y,Math.Abs(scale.X),Math.Abs(scale.Y));
					
								
					
						
				
									
				// THE MULTI SELECTION
				if(doMultiSelection)
					{
				//(node.Value.Top  >msCanvas.y  && node.Value.Top - node.Value.Height < msCanvas.w)*/
				if ((FOutputTransformed[index].x > msCanvas.x  && FOutputTransformed[index].x + FOutputTransformed[index].z < msCanvas.z )&&
					(FOutputTransformed[index].y < msCanvas.y && FOutputTransformed[index].y - FOutputTransformed[index].w > msCanvas.w))
				{
					node.Value.Select = true;
					isMultiSelection = true;
				}
				else
					node.Value.Select = false;
					}	
					
					
					
					
					FIsLocked[index] = node.Value.IsLocked;
					FSelection[index] = node.Value.Select;
					FMouseOverNode[index] = node.Value.MouseOver;
					FMouseOverHeadline[index] = node.Value.MouseOverHeadline;
					
					index++;
				}
			}
			// save this frames mouseclick for togedge calculation in next frame
			lastFrameMouseClick = (int)FMouseCanvas[0].z;
		}
	
       
			
	}
	
	public class Node
	{
		#region fields & get set
		private double _left;
		public double Left
		{
			get { return _left; }
			set { _left = value; }
		}
		
		private double _top;
		public double Top
		{
			get { return _top; }
			set { _top = value; }
		}
		
		private double _width;
		public double Width
		{
			get { return _width; }
			set { _width = value; }
		}
		
		private double _height;
		public double Height
		{
			get { return _height; }
			set { _height = value; }
		}
		
		private bool _select;
		public bool Select
		{
			get { return _select; }
			set { _select = value; }
		}
		
		private bool _mouseOver;
		public bool MouseOver
		{
			get { return _mouseOver; }
			set { _mouseOver = value; }
		}
		
		private bool _mouseOverHeadline;
		public bool MouseOverHeadline
		{
			get { return _mouseOverHeadline; }
			set { _mouseOverHeadline = value; }
		}
		
		private int _inputCount;
		public int InputCount
		{
			get { return _inputCount; }
			set { _inputCount = value; }
		}
		
		private int _outputCount;
		public int OutputCount
		{
			get { return _outputCount; }
			set { _outputCount = value; }
		}
		
		private bool _isLocked;
		public bool IsLocked
		{
			get { return _isLocked; }
			set { _isLocked = value; }
		}
		
		
		public Node (int left, int top, int width, int height, int inputCount, int outputCount)
		{
			_left = left;
			_top = top;
			_width = width;
			_height = height;
			_select = false;
			_inputCount = inputCount;
			_outputCount = outputCount;
			_isLocked = false;
			_mouseOverHeadline = false;
		}
		
		private void calcPinPostions (){
			
		}
		
		#endregion fields & get set
		
	}
}
